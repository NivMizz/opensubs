extends Node

# Connection variables

var NetworkENet : NetworkedMultiplayerENet

# Connection functions

func connect_to_server() -> void:
	print("Attempting to connect to server")
	NetworkENet = NetworkedMultiplayerENet.new()
	NetworkENet.create_client(Settings.ServerIp, Settings.ServerPort)
	get_tree().set_network_peer(NetworkENet)
	NetworkENet.connect("connection_succeeded", self, "connection_succeeded")
	NetworkENet.connect("connection_failed", self, "connection_failed")

func connection_succeeded() -> void:
	print("Connection successful")
	get_node("/root/Menu").connection_succeeded()

func connection_failed() -> void:
	print("Connection failed")
	get_node("/root/Menu").connection_failed()

func close_server_connection() -> void:
	print("Closing connection to server")
	get_node("/root/Menu").connection_failed()
	NetworkENet.close_connection()

remote func username_approved() -> void:
	print("Username approved")

remote func username_rejected() -> void:
	print("Username rejected")

# Main menu functions

remote func set_joined_games(games : Dictionary) -> void:
	get_node("/root/Menu/Panels/JoinedGames").set_games(games)

remote func set_public_games(games : Dictionary) -> void:
	get_node("/root/Menu/Panels/PublicGames").set_games(games)

remote func set_game_info(gameinfo : Dictionary) -> void:
	get_node("/root/Menu/Panels/GameInfo").set_game_info(gameinfo)

# Game functions

remote func set_game_variable(variable : String, value) -> void:
	if GameVariables.get(variable) != null:
		GameVariables.set(variable, value)
		match variable:
			"StartTime":
				if value != -1:
					if GameVariables.GameNode != null:
						GameVariables.GameNode.unpause_game()
			"GenerateTroopsTicksDivisor":
				GameVariables.GenerateTroopsTicks = GameVariables.DefaultGenerateTroopsTicks / GameVariables.GenerateTroopsTicksDivisor
			"GenerateMaximumShieldTicksDivisor":
				GameVariables.GenerateMaximumShieldTicks = GameVariables.DefaultGenerateMaximumShieldTicks / GameVariables.GenerateMaximumShieldTicksDivisor
	else:
		# Failure case
		print("\"" + variable + "\" is not a valid game variable")

remote func calculate_game_state() -> void:
	GameState.calculate_game_state()

remote func set_revealed_orders(globaltick : int, revealedorders : Dictionary) -> void:
	OrderFunctions.set_revealed_orders(globaltick, revealedorders)

remote func set_player(player : String, value : Dictionary) -> void:
	GameVariables.Players[player] = value
	if GameVariables.GameNode != null:
		GameVariables.GameNode.update_players()

remote func remove_online_player(player : String) -> void:
	GameVariables.OnlinePlayers.erase(player)
	GameVariables.GameNode.update_online_players()

remote func add_online_player(player : String) -> void:
	if GameVariables.OnlinePlayers.has(player) == false:
		GameVariables.OnlinePlayers.append(player)
	GameVariables.GameNode.update_online_players()

remote func add_message(chat : Array, message : Dictionary) -> void:
	if GameVariables.Chats.has(chat) == false:
		GameVariables.Chats[chat] = []
	GameVariables.Chats[chat].append(message)
	GameVariables.GameNode.update_messages(chat)

remote func set_order(ordertick : int, orderid : int, order : Dictionary) -> void:
	OrderFunctions.set_order(ordertick, orderid, order)

remote func remove_order(ordertick : int, orderid : int) -> void:
	var Order : Dictionary = GameVariables.Orders[ordertick][orderid]
	OrderFunctions.remove_order(Order)

remote func set_launch_order_troops(launchordertick : int, launchorderid : int, troops : int) -> void:
	var LaunchOrder : Dictionary = GameVariables.Orders[launchordertick][launchorderid]
	OrderFunctions.set_launch_order_troops(LaunchOrder, troops)

remote func focus_next_submarine() -> void:
	GameState.FocusNextSubmarine = true

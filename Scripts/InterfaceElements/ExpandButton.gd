extends Button

onready var PanelSize : Vector2 = get_parent().rect_size

var PreviousPosition : Vector2
var PressedCursorPosition : Vector2

func _process(delta : float):
	if pressed:
		var PanelBottom : float = get_parent().rect_position.y + get_parent().rect_size.y
		get_parent().rect_position.y = min(PreviousPosition.y + get_global_mouse_position().y - PressedCursorPosition.y, PanelBottom - PanelSize.y)
		get_parent().rect_size.y = PanelBottom - get_parent().rect_position.y

func prepare() -> void:
	var PanelBottom : float = get_parent().rect_position.y + get_parent().rect_size.y
	get_parent().rect_position.y = PanelBottom - PanelSize.y
	get_parent().rect_size.y = PanelSize.y

func _on_ExpandButton_button_down() -> void:
	PreviousPosition = get_parent().rect_position
	PressedCursorPosition = get_global_mouse_position()

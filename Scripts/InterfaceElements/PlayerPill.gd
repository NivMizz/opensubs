extends Panel

onready var CommunicationsPanel : Node = GameVariables.GameNode.get_node("Interface/Panels/Communications")

var Username : String
var Player : String
var UpdateMessages : bool = true

func set_player(player : String) -> void:
	Username = GameVariables.Players[player]["Username"]
	Player = VariableFunctions.get_username_player(Username)
	$PlayerLabel.text = Username
	rect_min_size.x = $PlayerLabel.get_font("font").get_string_size(Username).x + 10
	theme = theme.duplicate(true)
	get_stylebox("panel").bg_color = Color(GameVariables.Colors[player])

func _on_PlayerButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		if UpdateMessages:
			CommunicationsPanel.CurrentPlayers.append(Player)
			CommunicationsPanel.CurrentPlayers.sort()
			CommunicationsPanel.set_messages(CommunicationsPanel.CurrentPlayers)
		modulate.a = 1
	else:
		if UpdateMessages:
			CommunicationsPanel.CurrentPlayers.erase(Player)
			CommunicationsPanel.CurrentPlayers.sort()
			CommunicationsPanel.set_messages(CommunicationsPanel.CurrentPlayers)
		modulate.a = 0.6

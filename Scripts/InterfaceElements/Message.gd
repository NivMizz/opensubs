extends VBoxContainer

var PlayerPill : PackedScene = preload("res://Scenes/InterfaceElements/PlayerPill.tscn")

func set_message(message : Dictionary, ignoremouse : bool = false) -> void:
	for player in message["Players"]:
		var PlayerPillInstance := PlayerPill.instance()
		PlayerPillInstance.set_player(player)
		if ignoremouse:
			PlayerPillInstance.get_node("PlayerButton").mouse_filter = 2
		$Header/HBoxContainer.add_child(PlayerPillInstance)
	var TimeText : String = str(VariableFunctions.get_time_text(OS.get_unix_time() - message["Time"], true, 1))
	if TimeText != "Now":
		TimeText += " ago"
	$Header/TimeLabel.text = TimeText
	$TextLabel.text = message["Text"]

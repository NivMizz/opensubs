extends Panel

func set_player(player : String, playerinfo : Dictionary) -> void:
	if playerinfo["Occupied"]:
		$PlayerLabel.text = playerinfo["Username"]
		theme = theme.duplicate(true)
		get_stylebox("panel").bg_color = Color(GameVariables.Colors[player])

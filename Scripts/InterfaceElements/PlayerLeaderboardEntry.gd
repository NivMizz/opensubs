extends Control

var Player : String

func _ready():
	$ProgressBar.theme = $ProgressBar.theme.duplicate(true)

func set_player(player : String) -> void:
	Player = player
	$PlayerName/Label.text = GameVariables.Players[Player]["Username"]
	var PlayerColor : Color = Color(GameVariables.Colors[Player])
	$ProgressBar.get_stylebox("fg").bg_color = PlayerColor
	$ProgressBar.get_stylebox("bg").bg_color = PlayerColor.lightened(0.5)

func update_tick() -> void:
	var PlayerInfo : Dictionary = GameVariables.PlayersInfo[Player]
	var PlayerInfoHistory : Array = PlayerInfo["History"]
	var PlayerInfoHistoryEntry : Dictionary = PlayerInfoHistory[GameVariables.VisibleTick]
	var PlayerInfoType : String = get_node("/root/Game/Interface/Panels/Leaderboard").PlayerInfoType
	# Set interface elements
	match PlayerInfoType:
		"Troops":
			# Set player info
			var TroopsIncrease : int
			for outpost in GameVariables.OutpostData:
				var Data : Dictionary = GameVariables.OutpostData[outpost]
				var History : Array = Data["History"]
				var HistoryEntry : Dictionary = History[GameVariables.VisibleTick]
				if HistoryEntry["CurrentPlayer"] == Player:
					var CurrentType : String = HistoryEntry["CurrentType"]
					if CurrentType == "Factory":
						var GenerateTroopsTicks : int = ceil(GameVariables.GenerateTroopsTicks * HistoryEntry["TroopsTicksMultipliersTotal"])
						var GenerateTroopsNumber : int = ceil(GameVariables.GenerateTroopsNumber * HistoryEntry["TroopsNumberMultipliersTotal"])
						TroopsIncrease += floor(100.0 / float(GenerateTroopsTicks)) * GenerateTroopsNumber
			$PlayerInfo/Troops/Label.text = str(PlayerInfoHistoryEntry["TroopsTotal"]) + " of " + str(PlayerInfoHistoryEntry["ChargeTotal"]) + " (+" + str(TroopsIncrease) + " per 100 ticks)"
			# Get greatest player charge
			# TODO: move to panel
			var GreatestPlayerCharge : int = 1
			for chargeplayer in GameVariables.PlayersInfo:
				var ChargePlayerData : Dictionary = GameVariables.PlayersInfo[chargeplayer]
				var ChargePlayerHistory : Array = ChargePlayerData["History"]
				var ChargePlayerHistoryEntry : Dictionary = ChargePlayerHistory[GameVariables.VisibleTick]
				if ChargePlayerHistoryEntry["ChargeTotal"] > GreatestPlayerCharge:
					GreatestPlayerCharge = ChargePlayerHistoryEntry["ChargeTotal"]
			# Set progress bar
			$ProgressBar.rect_size.x = float(PlayerInfoHistoryEntry["ChargeTotal"]) / float(GreatestPlayerCharge) * rect_size.x
			$ProgressBar.max_value = PlayerInfoHistoryEntry["ChargeTotal"]
			$ProgressBar.value = PlayerInfoHistoryEntry["TroopsTotal"]
		"Outposts":
			# Set player info
			var TotalPlayerOutposts : int
			var PlayerOutpostTypesCount : Dictionary
			for outpost in GameVariables.OutpostData:
				var Data : Dictionary = GameVariables.OutpostData[outpost]
				var History : Array = Data["History"]
				var HistoryEntry : Dictionary = History[GameVariables.VisibleTick]
				if HistoryEntry["CurrentPlayer"] == Player:
					var CurrentType : String = HistoryEntry["CurrentType"]
					if PlayerOutpostTypesCount.has(CurrentType) == false:
						PlayerOutpostTypesCount[CurrentType] = 0
					PlayerOutpostTypesCount[CurrentType] += 1
					TotalPlayerOutposts += 1
			for child in $PlayerInfo/Outposts.get_children():
				child.get_node("Label").text = "x" + str(PlayerOutpostTypesCount[child.name])
			# Set progress bar
			$ProgressBar.rect_size.x = rect_size.x
			$ProgressBar.max_value = (len(GameVariables.Players) - 1) * GameVariables.OutpostsPerPlayer
			$ProgressBar.value = TotalPlayerOutposts
	# Set player info visible
	for child in $PlayerInfo.get_children():
		child.visible = child.name == PlayerInfoType

extends Control

var Players : Array

func _process(delta : float):
	rect_min_size.y = $Message.rect_size.y + 20

func set_message(message : Dictionary) -> void:
	var OtherPlayers : Array = Players.duplicate(true)
	OtherPlayers.erase(GameVariables.Player)
	var OtherPlayersMessage : Dictionary = message.duplicate(true)
	OtherPlayersMessage["Players"] = OtherPlayers
	$Message.set_message(OtherPlayersMessage, true)

func _on_ChatButton_pressed() -> void:
	GameVariables.GameNode.get_node("Interface/Panels/Communications").set_chat(Players)

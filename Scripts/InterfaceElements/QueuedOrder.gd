extends Control

var Order : Dictionary

func _process(delta : float) -> void:
	# Set time to order text
	var TimeToOrder : int = OrderFunctions.get_time_to_order(Order)
	var TimeToOrderText : String = VariableFunctions.get_time_text(TimeToOrder)
	var ButtonText : String = Order["Type"] + " (" + TimeToOrderText + ")"
	# Set button elements
	for child in get_children():
		child.text = ButtonText

func prepare() -> void:
	# Set button elements
	for child in get_children():
		for secondarychild in child.get_node("OrderIcons").get_children():
			secondarychild.visible = secondarychild.name == Order["Type"]
	# Set order button visibility
	update_order_status()

func update_order_status() -> void:
	var VisibleOrderButton : Node = $ValidOrderButton
	# Check if order is invalid
	if OrderFunctions.get_order_invalid(Order):
		VisibleOrderButton = $InvalidOrderButton
	# Check if order is warning
	elif OrderFunctions.get_order_warning(Order):
		VisibleOrderButton = $WarningOrderButton
	# Set button visibility
	for child in get_children():
		child.visible = child == VisibleOrderButton

func _on_OrderButton_pressed() -> void:
	# Jump to order tick
	var PreviousJumping : bool = GameVariables.GameNode.Jumping
	GameVariables.GameNode.Jumping = true
	GameVariables.GameNode.set_tick(Order["Tick"])
	GameVariables.GameNode.Jumping = PreviousJumping
	# Focus order
	match Order["Type"]:
		"Launch":
			var Submarine : String = OrderFunctions.get_submarine_from_launch_order(Order)
			GameVariables.GameNode.focus_submarine(Submarine, true)
		"Gift":
			var LaunchOrder : Dictionary = GameVariables.Orders[Order["LaunchOrderTick"]][Order["LaunchOrderId"]]
			var Submarine : String = OrderFunctions.get_submarine_from_launch_order(LaunchOrder)
			GameVariables.GameNode.focus_submarine(Submarine, true)

func _on_CancelButton_pressed() -> void:
	# Get orders for removal
	var RemovalOrders : Array = [Order]
	if Order["Type"] == "Launch":
		var Submarine : String = OrderFunctions.get_submarine_from_launch_order(Order)
		RemovalOrders = OrderFunctions.get_all_submarine_orders(Submarine)
	# Remove orders
	for order in RemovalOrders:
		Network.rpc_id(
			1,
			"remove_order",
			# Game ID
			GameVariables.GameId,
			# Client ID
			get_tree().get_network_unique_id(),
			# Tick
			order["Tick"],
			# Order ID
			order["OrderId"]
		)
		OrderFunctions.remove_order(order)

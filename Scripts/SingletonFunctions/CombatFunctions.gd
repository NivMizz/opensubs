extends Node

# Data variables

# Checks if the passed submarine should affect others
func get_submarine_affects(tick : int, submarine : String) -> bool:
	if GameVariables.SubmarineData.has(submarine):
		var Data : Dictionary = GameVariables.SubmarineData[submarine]
		var LaunchOrder : Dictionary = GameVariables.Orders[Data["LaunchOrderTick"]][Data["LaunchOrderId"]]
		var History : Array = Data["History"]
		if OrderFunctions.get_order_invalid(LaunchOrder) == false:
			var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(tick, History)
			if tick <= GameVariables.GlobalTick or HistoryEntry["VisibleTotal"] > 0:
				return true
	return false

# Sets submarine as active
func set_submarine_active(tick : int, submarine : String) -> void:
	if GameVariables.ActiveSubmarines.has(submarine) == false:
		# Gets data
		var Data : Dictionary = GameVariables.SubmarineData[submarine]
		var History : Array = Data["History"]
		# Sets submarine in active submarines
		GameVariables.ActiveSubmarines.append(submarine)
		GameVariables.CurrentActiveSubmarines.append(submarine)
		# Removes future submarine combat
		DataFunctions.remove_data_future_list_entries(tick, Data, "Total", "Troops", "TroopsKeys", "TroopsTotal")
		DataFunctions.remove_data_future_list_entries(tick, Data, "CurrentItem", "Players", "PlayersKeys", "CurrentPlayer")

# Calculates submarine combat
func calculate_submarine_combat(tick : int, submarine : String) -> void:
	# Get submarine data
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	# Set submarine attacked
	if get_submarine_affects(tick, submarine): 
		for combatsubmarine in get_combat_submarines(tick, submarine):
			# Set combat submarine attack
			set_submarine_attacked(tick, submarine, combatsubmarine)
			# Add combat submarine to combat submarines
			DataFunctions.set_data_combat_submarine(tick, Data, combatsubmarine)

# Gets submarines that will engange in combat with passed submarine at passed tick
func get_combat_submarines(tick : int, submarine : String) -> Array:
	# Get submarine data
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var History : Array = Data["History"]
	var HistoryIndex : int = HistoryFunctions.get_history_index(tick, History)
	var PreviousHistoryEntry : Dictionary = History[HistoryIndex - 1]
	var CurrentHistoryEntry : Dictionary = History[HistoryIndex]
	# Get all submarines passed submarine intersects with
	var CombatSubmarines : Array
	for activesubmarine in GameVariables.SubmarineData:
		# Check that active submarine is not submarine
		if activesubmarine != submarine:
			# Get opponent submarine data
			var ActiveSubmarineData : Dictionary = GameVariables.SubmarineData[activesubmarine]
			var ActiveSubmarineHistory : Array = ActiveSubmarineData["History"]
			var ActiveSubmarineHistoryIndex : int = HistoryFunctions.get_history_index(tick, ActiveSubmarineHistory)
			# Check that active submarine is present for one tick before passed tick
			if ActiveSubmarineHistoryIndex >= 1:
				# Get active submarine history entries
				var ActiveSubmarinePreviousHistoryEntry : Dictionary = ActiveSubmarineHistory[ActiveSubmarineHistoryIndex - 1]
				var ActiveSubmarineCurrentHistoryEntry : Dictionary = ActiveSubmarineHistory[ActiveSubmarineHistoryIndex]
				# Checks if the submarine's target is the same as actuve submarine's initial outpost and vice versa
				if PreviousHistoryEntry["CurrentTarget"] == ActiveSubmarineData["InitialOutpost"] and ActiveSubmarinePreviousHistoryEntry["CurrentTarget"] == Data["InitialOutpost"]:
					# Checks if the submarines do not belong to the same player
					if PreviousHistoryEntry["CurrentPlayer"] != ActiveSubmarinePreviousHistoryEntry["CurrentPlayer"]:
						# Get previous and current positions
						var PreviousPosition : Vector2 = PreviousHistoryEntry["Position"]
						var CurrentPostion : Vector2 = CurrentHistoryEntry["Position"]
						var ActiveSubmarinePreviousPosition : Vector2 = ActiveSubmarinePreviousHistoryEntry["Position"]
						var ActiveSubmarineCurrentPosition : Vector2 = ActiveSubmarineCurrentHistoryEntry["Position"]
						# Get active submarine positions wrapped to submarine positions
						var ActiveSubmarinePreviousWrappedPosition : Vector2 = MapFunctions.get_closest_wrapped_position(PreviousPosition, ActiveSubmarinePreviousPosition)
						var ActiveSubmarineCurrentWrappedPosition : Vector2 = MapFunctions.get_closest_wrapped_position(CurrentPostion, ActiveSubmarineCurrentPosition)
						# Checks if submarine positions overlap active submarine positions
						if get_line_overlap([PreviousPosition, CurrentPostion], [ActiveSubmarinePreviousWrappedPosition, ActiveSubmarineCurrentWrappedPosition]):
							var CombatTick : int = CurrentHistoryEntry["Tick"]
							# Check that submarine affects
							if get_submarine_affects(CombatTick, activesubmarine):
								CombatSubmarines.append(activesubmarine)
							break
	# TODO: reimplement
	# Sorts intersecting submarines ticks
#	for intersectingsubmarinetick in IntersectingSubmarines:
#		IntersectingSubmarines[intersectingsubmarinetick].sort_custom(self, "sort_data_troops")
	return CombatSubmarines

# Checks if segments of a submarine path overlap
func get_line_overlap(firstline : Array, secondline : Array) -> bool:
	var FirstLineRect : Rect2 = Rect2(firstline[0], Vector2()).expand(firstline[1])
	var SecondLineRect : Rect2 = Rect2(secondline[0], Vector2()).expand(secondline[1])
	return FirstLineRect.intersects(SecondLineRect, true)

# Calculates submarine's effect
func calculate_submarine_effect(submarine : String, tick : int = -1) -> void:
	# Get submarine data
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var LaunchOrder : Dictionary = GameVariables.Orders[Data["LaunchOrderTick"]][Data["LaunchOrderId"]]
	var History : Array = Data["History"]
	# Get start tick
	var StartTick : int = LaunchOrder["Tick"]
	if tick != -1:
		StartTick = tick
	# Calculates submarine effect
	if GameVariables.InitialCalculation == false:
		# Resets active items
		GameVariables.ActiveOutposts = []
		GameVariables.ActiveSubmarines = []
		GameVariables.CurrentActiveSubmarines = []
		# Sets submarine active
		set_submarine_active(StartTick, submarine)
		# Calculates submarine initial outpost effect
		calculate_submarine_initial_outpost_effect(submarine)
		# Calculates submarine combat and arrival
		for i in GameVariables.VisibleTickMaximum - StartTick:
			var CurrentTick : int = StartTick + i
			# TODO: add verify launch order invalid
			# Calculates active submarines history entry
			GameState.calculate_active_submarines_history_entry(CurrentTick)
			# Calculates active outposts military generation
			for outpost in GameVariables.ActiveOutposts:
				GenerationFunctions.calculate_outpost_entry_military_generation(i, outpost)
	else:
		# Calculates submarine initial outpost effect
		calculate_submarine_initial_outpost_effect(submarine)

func calculate_submarine_initial_outpost_effect(submarine : String) -> void:
	# Gets data
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var History : Array = Data["History"]
	var LaunchOrder : Dictionary = GameVariables.Orders[Data["LaunchOrderTick"]][Data["LaunchOrderId"]]
	var InitialOutpost : String = LaunchOrder["InitialOutpost"]
	var InitialOutpostData : Dictionary = GameVariables.OutpostData[InitialOutpost]
	# Get start tick
	var StartTick : int = History[0]["Tick"]
	# Sets effected items active
	if GameVariables.InitialCalculation == false:
		GenerationFunctions.set_outpost_active(StartTick, InitialOutpost)
		set_outpost_launched_submarines_active(StartTick, InitialOutpost)
	# Set initial outpost troops taken
	if get_submarine_affects(StartTick, submarine):
		DataFunctions.set_data_troops(StartTick, InitialOutpostData, submarine, -LaunchOrder["InitialTroops"])

func calculate_submarine_target_outpost_effect(submarine : String) -> void:
	# Gets data
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var History : Array = Data["History"]
	var Target : String = History[-1]["CurrentTarget"]
	var TargetData : Dictionary = GameVariables.OutpostData[Target]
	# Get ticks
	var EndTick : int = History[-1]["Tick"]
	var ArrivalTick : int = EndTick + 1
	# Calculate submarine effect
	if ArrivalTick < GameVariables.VisibleTickMaximum:
		# Sets effected items active
		if GameVariables.InitialCalculation == false:
			GenerationFunctions.set_outpost_active(ArrivalTick, Target)
			set_outpost_launched_submarines_active(ArrivalTick, Target)
		# Set target outpost attacked
		if get_submarine_affects(EndTick, submarine):
			set_outpost_attacked(ArrivalTick, Target, submarine)
		# Calculates dependent orders warning
		for order in OrderFunctions.get_all_submarine_orders(submarine):
			OrderFunctions.calculate_order_warning(order)

# Sets outpost future launched submarines as active
func set_outpost_launched_submarines_active(tick : int, outpost : String) -> void:
	# Get outpost data
	var OutpostData : Dictionary = GameVariables.OutpostData[outpost]
	# Set launched submarines as active
	var InvertedLaunchedSubmarinesKeys : Array = OutpostData["LaunchedSubmarinesKeys"].duplicate(true)
	InvertedLaunchedSubmarinesKeys.invert()
	for launchedsubmarinetick in InvertedLaunchedSubmarinesKeys:
		if tick <= launchedsubmarinetick:
			for launchedsubmarine in OutpostData["LaunchedSubmarines"][launchedsubmarinetick]:
				# Get launched submarine data
				var LaunchedSubmarineData : Dictionary = GameVariables.SubmarineData[launchedsubmarine]
				var LaunchedSubmarineLaunchOrder : Dictionary = GameVariables.Orders[LaunchedSubmarineData["LaunchOrderTick"]][LaunchedSubmarineData["LaunchOrderId"]]
				var LaunchedSubmarineHistory : Array = LaunchedSubmarineData["History"]
				# Set launched submarine active
				if LaunchedSubmarineData["InitialOutpost"] == outpost or LaunchedSubmarineHistory[-1]["CurrentTarget"] == outpost:
					set_submarine_active(launchedsubmarinetick, launchedsubmarine)
		else:
			break

# Calculates outpost history after attack
func set_outpost_attacked(tick : int, outpost : String, attacksubmarine : String) -> void:
	# Get name of combat for player info
	var PlayerInfoCombat : String = outpost + "/" + attacksubmarine
	print("Set outpost attacked " + PlayerInfoCombat + " " + str(tick))
	# Get outpost data
	var OutpostData : Dictionary = GameVariables.OutpostData[outpost]
	var OutpostHistory : Array = OutpostData["History"]
	var OutpostHistoryEntry : Dictionary = OutpostHistory[tick]
	# Get attack submarine data
	var AttackSubmarineData : Dictionary = GameVariables.SubmarineData[attacksubmarine]
	var AttackSubmarineHistory : Array = AttackSubmarineData["History"]
	var AttackSubmarineHistoryEntry : Dictionary = AttackSubmarineHistory[-1]
	# Check if attack submarine should affect outpost
	if get_submarine_affects(AttackSubmarineHistory[-1]["Tick"], attacksubmarine):
		# Get outpost variables
		var OutpostCurrentPlayer : String = OutpostHistoryEntry["CurrentPlayer"]
		var OutpostDormant : bool = OutpostHistoryEntry["CurrentPlayer"] == "Dormant"
		var OutpostPreviousSonarStatus : bool = SonarFunctions.get_sonar_item_status(tick, outpost)
		# Get attack submarine variables
		var AttackSubmarinePlayer : String = AttackSubmarineHistoryEntry["CurrentPlayer"]
		var AttackSubmarineTroops : int = AttackSubmarineHistoryEntry["TroopsTotal"]
		var AttackSubmarineGift : bool = AttackSubmarineHistoryEntry["CurrentGift"]
		var AttackSubmarinePeaceful : bool = AttackSubmarinePlayer == OutpostCurrentPlayer or AttackSubmarineGift
		# Calculate attack submarine arrival
		if AttackSubmarinePeaceful or OutpostDormant:
			# Set outpost data
			DataFunctions.set_data_troops(tick, OutpostData, attacksubmarine, AttackSubmarineTroops)
			if OutpostDormant:
				DataFunctions.set_data_player(tick, OutpostData, attacksubmarine, AttackSubmarinePlayer)
		else:
			# Get lost values
			var LostShield : int = AttackSubmarineTroops
			var LostTroops : int = AttackSubmarineTroops - OutpostHistoryEntry["ShieldTotal"]
			var ClampedLostShield : int = min(LostShield, OutpostHistoryEntry["ShieldTotal"])
			var ClampedLostTroops : int = min(LostTroops, OutpostHistoryEntry["TroopsTotal"])
			var OutpostCaptured : bool = LostTroops >= OutpostHistoryEntry["TroopsTotal"]
			# Set outpost data
			DataFunctions.set_data_shield(tick, OutpostData, attacksubmarine, -ClampedLostShield)
			DataFunctions.set_data_troops(tick, OutpostData, attacksubmarine, -ClampedLostTroops * 2 + LostTroops)
			if OutpostCaptured:
				DataFunctions.set_data_player(tick, OutpostData, attacksubmarine, AttackSubmarinePlayer)
			# Set outpost player info
			var PlayerInfo : Dictionary = GameVariables.PlayersInfo[OutpostCurrentPlayer]
			DataFunctions.set_data_troops(tick, PlayerInfo, PlayerInfoCombat, -ClampedLostTroops)
			# Set attack submarine player info
			var AttackSubmarinePlayerInfo : Dictionary = GameVariables.PlayersInfo[AttackSubmarinePlayer]
			DataFunctions.set_data_troops(tick, AttackSubmarinePlayerInfo, PlayerInfoCombat, -(ClampedLostTroops + ClampedLostShield))
		# Updates outpost sonar
		var OutpostCurrentSonarStatus : bool = SonarFunctions.get_sonar_item_status(tick, outpost)
		if OutpostCurrentSonarStatus != OutpostPreviousSonarStatus:
			SonarFunctions.update_item_sonar(tick, outpost)

# Calculates submarine history after attack
func set_submarine_attacked(tick : int, submarine : String, attacksubmarine : String) -> void:
	# Get name of combat for player info
	var PlayerInfoCombat : String = submarine + "/" + attacksubmarine
	print("Set submarine attacked " + PlayerInfoCombat + " " + str(tick))
	# Get submarine data
	var SubmarineData : Dictionary = GameVariables.SubmarineData[submarine]
	var SubmarineHistory : Array = SubmarineData["History"]
	var SubmarineHistoryEntry : Dictionary = HistoryFunctions.get_history_entry(tick, SubmarineHistory)
	var SubmarineGift : bool = SubmarineHistoryEntry["CurrentGift"]
	# Get attack submarine data
	var AttackSubmarineData : Dictionary = GameVariables.SubmarineData[attacksubmarine]
	var AttackSubmarineHistory : Array = AttackSubmarineData["History"]
	var AttackSubmarineHistoryEntry : Dictionary = HistoryFunctions.get_history_entry(tick, AttackSubmarineHistory)
	var AttackSubmarineGift : bool = AttackSubmarineHistoryEntry["CurrentGift"]
	# Calculate attack submarine combat
	if SubmarineGift and AttackSubmarineGift == false:
		DataFunctions.set_data_player(tick, SubmarineData, attacksubmarine, AttackSubmarineHistoryEntry["CurrentPlayer"])
	elif SubmarineGift == false and AttackSubmarineGift == false:
		# Get lost values
		var LostTroops : int = min(SubmarineHistoryEntry["TroopsTotal"], AttackSubmarineHistoryEntry["TroopsTotal"])
		var SubmarineCaptured : bool = LostTroops >= SubmarineHistoryEntry["TroopsTotal"]
		# Set submarine data
		DataFunctions.set_data_troops(tick, SubmarineData, attacksubmarine, -LostTroops)
		if SubmarineCaptured:
			var AttackSubmarineCurrentPlayer : String = AttackSubmarineHistoryEntry["CurrentPlayer"]
			DataFunctions.set_data_player(tick, SubmarineData, attacksubmarine, AttackSubmarineCurrentPlayer)
		# Set player info
		var SubmarineCurrentPlayer : String = SubmarineHistoryEntry["CurrentPlayer"]
		var SubmarinePlayerInfo : Dictionary = GameVariables.PlayersInfo[SubmarineCurrentPlayer]
		DataFunctions.set_data_troops(tick, SubmarinePlayerInfo, PlayerInfoCombat, -LostTroops)
	# Sets attack submarine active
	set_submarine_active(tick, attacksubmarine)

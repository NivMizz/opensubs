extends Node

# Sets outpost player outposts as active
func set_outpost_active(tick : int, outpost : String) -> void:
	# Get data
	var OutpostData : Dictionary = GameVariables.OutpostData[outpost]
	var OutpostHistory : Array = OutpostData["History"]
	var OutpostHistoryEntry : Dictionary = OutpostHistory[tick]
	# Get player outposts
	var PlayerOutposts : Array
	var CurrentPlayer : String = OutpostHistoryEntry["CurrentPlayer"]
	if CurrentPlayer == "Dormant":
		# Sets outpost as active
		PlayerOutposts.append(outpost)
	else:
		# Sets player outposts as active
		for playeroutpost in GameVariables.OutpostData:
			# Get player outpost data
			var PlayerOutpostData : Dictionary = GameVariables.OutpostData[playeroutpost]
			var PlayerOutpostHistory : Array = PlayerOutpostData["History"]
			var PlayerOutpostHistoryEntry : Dictionary = PlayerOutpostHistory[tick]
			# Sets player outpost as active
			var PlayerOutpostCurrentPlayer : String = PlayerOutpostHistoryEntry["CurrentPlayer"]
			if PlayerOutpostCurrentPlayer == CurrentPlayer:
				PlayerOutposts.append(playeroutpost)
	# Set player outposts active
	for playeroutpost in PlayerOutposts:
		if GameVariables.ActiveOutposts.has(playeroutpost) == false:
			# Gets player outpost data
			var PlayerOutpostData : Dictionary = GameVariables.OutpostData[playeroutpost]
			# Sets player outpost in active outposts
			GameVariables.ActiveOutposts.append(playeroutpost)
			# Removes future player outpost generation
			DataFunctions.remove_data_future_list_entries(tick, PlayerOutpostData, "Total", "Troops", "TroopsKeys", "TroopsTotal")
			DataFunctions.remove_data_future_list_entries(tick, PlayerOutpostData, "Total", "Shield", "ShieldKeys", "ShieldTotal")
			DataFunctions.remove_data_future_list_entries(tick, PlayerOutpostData, "Current", "Players", "PlayersKeys", "CurrentPlayer")

# Calculates military generation for an outpost
func calculate_outpost_military_generation(starttick : int, endtick : int, outpost : String) -> void:
	# Calculate outpost entry military generation for every entry from start tick to end tick
	for i in endtick - starttick:
		var CurrentTick : int = starttick + i + 1
		calculate_outpost_entry_military_generation(CurrentTick, outpost)

# Calculates outpost shield and troop generation for a single history entry
func calculate_outpost_entry_military_generation(tick : int, outpost : String) -> void:
	# Get data
	var Data : Dictionary = GameVariables.OutpostData[outpost]
	var History : Array = Data["History"]
	# Get ticks
	var CurrentTick : int = tick
	var PreviousTick : int = max(tick - 1, 0)
	# Get history entries
	var CurrentHistoryEntry : Dictionary = History[CurrentTick]
	var PreviousHistoryEntry : Dictionary = History[PreviousTick]
	# Get military generation ticks
	var GenerateTroopsTicks : int = ceil(GameVariables.GenerateTroopsTicks * CurrentHistoryEntry["TroopsTicksMultipliersTotal"])
	var GenerateSingleShieldTicks : int = ceil(GameVariables.GenerateMaximumShieldTicks / Data["InitialShieldMaximum"] / CurrentHistoryEntry["ShieldMaximumMultipliersTotal"])
	var TroopsTicksCountdown : int = GenerateTroopsTicks
	var ShieldTicksCountdown : int = GenerateSingleShieldTicks
	# Calculate history entry
	var CurrentPlayer : String = CurrentHistoryEntry["CurrentPlayer"]
	if CurrentPlayer != "Dormant":
		# Get player info
		var PlayerInfo : Dictionary = GameVariables.PlayersInfo[CurrentPlayer]
		var PlayerInfoHistory : Array = PlayerInfo["History"]
		var PlayerInfoHistoryEntry : Dictionary = PlayerInfoHistory[tick]
		# Get troops countdown
		if CurrentHistoryEntry["CurrentType"] == "Factory":
			var LoweredTroopsTimeCountdown = GenerateTroopsTicks
			if tick != 0:
				LoweredTroopsTimeCountdown = PreviousHistoryEntry["TroopsTicksCountdown"] - 1
			TroopsTicksCountdown = LoweredTroopsTimeCountdown + GenerateTroopsTicks * int(LoweredTroopsTimeCountdown < 0)
		# Set troops increase
		if TroopsTicksCountdown == 0:
			var TroopsIncreaseMaximum : int = max(PlayerInfoHistoryEntry["ChargeTotal"] - PlayerInfoHistoryEntry["TroopsTotal"], 0)
			var TroopsIncrease : int = min(ceil(GameVariables.GenerateTroopsNumber * CurrentHistoryEntry["TroopsNumberMultipliersTotal"]), TroopsIncreaseMaximum)
			# Set outpost data
			DataFunctions.set_data_troops(tick, Data, outpost, TroopsIncrease)
			# Set player info
			DataFunctions.set_data_troops(tick, PlayerInfo, outpost, TroopsIncrease)
			# Set previous troops generation tick
			Data["PreviousTroopsGenerationTick"] = CurrentTick
		# Get shield countdown
		var LoweredShieldTimeCountdown = GenerateSingleShieldTicks
		if tick != 0:
			LoweredShieldTimeCountdown = PreviousHistoryEntry["ShieldTicksCountdown"] - 1
		ShieldTicksCountdown = LoweredShieldTimeCountdown + GenerateSingleShieldTicks * int(LoweredShieldTimeCountdown < 0)
		# Set shield increase
		if ShieldTicksCountdown == 0:
			var ShieldIncreaseMaximum : int = Data["InitialShieldMaximum"] - PreviousHistoryEntry["ShieldTotal"]
			var ShieldIncrease : int = min(1, ShieldIncreaseMaximum)
			DataFunctions.set_data_shield(tick, Data, outpost, ShieldIncrease)
			Data["PreviousSGenerationTick"] = CurrentTick
	# Sets history entry
	CurrentHistoryEntry["TroopsTicksCountdown"] = TroopsTicksCountdown
	CurrentHistoryEntry["ShieldTicksCountdown"] = ShieldTicksCountdown

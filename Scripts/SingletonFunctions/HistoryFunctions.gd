extends Node

# Tick functions

# Returns the index in history for passed tick
func get_history_index(tick : int, history : Array) -> int:
	var HistoryIndex : int = tick - history[0]["Tick"]
	if HistoryIndex >= 0 and HistoryIndex < len(history):
		return HistoryIndex
	else:
		# Failure case
		return -1

# Returns the entry in history for passed tick
func get_history_entry(tick : int, history : Array) -> Dictionary:
	var HistoryIndex : int = get_history_index(tick, history)
	if HistoryIndex != -1:
		return history[HistoryIndex]
	else:
		# Failure case
		return {}

# Variable functions

# Expands histories
func calculate_history_expansion() -> void:
	for player in GameVariables.PlayersInfo:
		calculate_player_info_expansion(player)
	for outpost in GameVariables.OutpostData:
		calculate_outpost_history_expansion(outpost)
	for submarine in GameVariables.SubmarineData:
		calculate_submarine_history_expansion(submarine)

# Expands player info
func calculate_player_info_expansion(player : String) -> void:
	# Get player info
	var PlayerInfo : Dictionary = GameVariables.PlayersInfo[player]
	var PlayerInfoHistory : Array = PlayerInfo["History"]
	var StartHistoryEntry : Dictionary = PlayerInfoHistory[-1]
	var StartTick : int = StartHistoryEntry["Tick"]
	# Set new player info entries
	for i in GameVariables.VisibleTickMaximum - StartTick:
		var CurrentTick : int = StartTick + i + 1
		var NewPlayerInfoEntry : Dictionary = PlayerInfoHistory[-1].duplicate(true)
		NewPlayerInfoEntry["Tick"] = CurrentTick
		PlayerInfoHistory.append(NewPlayerInfoEntry)

# Expands outpost history
func calculate_outpost_history_expansion(outpost : String) -> void:
	# Get outpost data
	var Data : Dictionary = GameVariables.OutpostData[outpost]
	var History : Array = Data["History"]
	var StartHistoryEntry : Dictionary = History[-1]
	var StartTick : int = StartHistoryEntry["Tick"]
	# Set new history entries
	for i in GameVariables.VisibleTickMaximum - StartTick:
		var CurrentTick : int = StartTick + i + 1
		var NewHistoryEntry : Dictionary = History[-1].duplicate(true)
		NewHistoryEntry["Tick"] = CurrentTick
		History.append(NewHistoryEntry)
	# Calcuclate outpost military generation until visible tick maximum
	GenerationFunctions.calculate_outpost_military_generation(
		StartTick,
		GameVariables.VisibleTickMaximum - 1,
		outpost
	)

# Expands submarine history
func calculate_submarine_history_expansion(submarine : String) -> void:
	# Get submarine data
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var History : Array = Data["History"]
	var StartHistoryEntry : Dictionary = History[-1]
	var StartTick : int = StartHistoryEntry["Tick"]
	# Check that submarine has not arrived already
	if StartTick >= GameVariables.VisibleTickMaximum:
		# Add submarine to active submarines
		GameVariables.ActiveSubmarines.append(submarine)
		# Calculate submarine positions
		PositionFunctions.calculate_submarine_all_positions(StartTick, submarine)

# Updates history total
func update_history_total(tick : int, data : Dictionary, total : String, previoussourcesvalue : int, currentsourcevalue : int) -> void:
	var History : Array = data["History"]
	var HistoryIndex : int = get_history_index(tick, History)
	for i in len(History) - HistoryIndex:
		var CurrentHistoryIndex : int = HistoryIndex + i
		var HistoryEntry : Dictionary = History[CurrentHistoryIndex]
		HistoryEntry[total] = HistoryEntry[total] - previoussourcesvalue + currentsourcevalue

# Updates history current item
func update_history_current_item(tick : int, data : Dictionary, list : String, listkeys : String, current : String) -> void:
	# Get data
	var History : Array = data["History"]
	var HistoryIndex : int = get_history_index(tick, History)
	for i in len(History) - HistoryIndex:
		var CurrentHistoryIndex : int = HistoryIndex + i
		var HistoryEntry : Dictionary = History[CurrentHistoryIndex]
		# Get current item
		var CurrentValue
		var InvertedListKeys : Array = data[listkeys].duplicate(true)
		InvertedListKeys.invert()
		for itemtick in InvertedListKeys:
			if itemtick <= HistoryEntry["Tick"]:
				CurrentValue = data[list][itemtick].values()[-1]
				break
		# Set current item
		HistoryEntry[current] = CurrentValue

# Updates history current order
func update_history_current_order(tick : int, data : Dictionary, list : String, listkeys : String, current : String) -> void:
	# Get data
	var History : Array = data["History"]
	var HistoryIndex : int = get_history_index(tick, History)
	for i in len(History) - HistoryIndex:
		var CurrentHistoryIndex : int = HistoryIndex + i
		var HistoryEntry : Dictionary = History[CurrentHistoryIndex]
		# Get current order
		var CurrentValue
		var InvertedListKeys : Array = data[listkeys].duplicate(true)
		InvertedListKeys.invert()
		for ordertick in InvertedListKeys:
			if ordertick <= HistoryEntry["Tick"]:
				for orderid in data[list][ordertick]:
					var Order : Dictionary = GameVariables.Orders[ordertick][orderid]
					if Order["Type"] == "Launch" or OrderFunctions.get_order_invalid(Order) == false:
						CurrentValue = data[list][ordertick][orderid]
						break
			if CurrentValue != null:
				break
		# Set current order
		HistoryEntry[current] = CurrentValue

# Sets the history boolean
func set_history_boolean(tick : int, history : Array, boolean : String, value : bool) -> void:
	var HistoryIndex : int = get_history_index(tick, history)
	for i in len(history) - HistoryIndex:
		var CurrentHistoryIndex : int = HistoryIndex + i
		history[CurrentHistoryIndex][boolean] = value

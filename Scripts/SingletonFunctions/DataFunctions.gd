extends Node

# Calculate data functions

# Calculates players info
func calculate_players_info() -> void:
	var PlayerInfoHistory : Array = [
		{
			"Tick" : 0,
			"TroopsTotal" : 0,
			"ChargeTotal" : 0
		}
	]
	var PlayerInfo : Dictionary = {
		"Type" : "PlayerInfo",
		"Troops" : {},
		"TroopsKeys" : [],
		"Charge" : {},
		"ChargeKeys" : [],
		"History" : PlayerInfoHistory
	}
	for player in GameVariables.Players:
		if player != "Dormant":
			GameVariables.PlayersInfo[player] = PlayerInfo.duplicate(true)

# Calculates the data for an outpost
func calculate_outpost_data(outpost : String, outpostinfo : Dictionary) -> void:
	# Troops variables
	var GenerateTroopsTicks : int = GameVariables.DefaultGenerateTroopsTicks / GameVariables.GenerateTroopsTicksDivisor
	# Shield variables
	var GenerateMaximumShieldTicks : int = GameVariables.DefaultGenerateMaximumShieldTicks / GameVariables.GenerateMaximumShieldTicksDivisor
	var GenerateSingleShieldTicks : int = GenerateMaximumShieldTicks / outpostinfo["InitialShieldMaximum"]
	# Get outpost visible
	var Visible : Dictionary
	var VisibleKeys : Array
	var VisibleTotal : int
	if outpostinfo["InitialPlayer"] == GameVariables.Player:
		VariableFunctions.set_keys_list(Visible, VisibleKeys, 0, outpost, 1)
		VisibleTotal = 1
	# Get outpost history
	var HistoryEntry : Dictionary = {
		"Tick" : 0,
		# Countdown variables
		"ShieldTicksCountdown" : GenerateSingleShieldTicks,
		"TroopsTicksCountdown" : GenerateTroopsTicks,
		# Total variables
		"VisibleTotal" : VisibleTotal,
		"TroopsTotal" : outpostinfo["InitialTroops"],
		"ShieldTotal" : 0,
		"TroopsTicksMultipliersTotal" : 1,
		"TroopsNumberMultipliersTotal" : 1,
		"ShieldMaximumMultipliersTotal" : 1,
		"SonarRangeMultipliersTotal" : 1,
		# Current variables
		"CurrentPlayer" : outpostinfo["InitialPlayer"],
		"CurrentType" : outpostinfo["InitialType"],
	}
	var History : Array
	for i in GameVariables.VisibleTickMaximum:
		var NewHistoryEntry : Dictionary = HistoryEntry.duplicate(true)
		NewHistoryEntry["Tick"] = i
		History.append(NewHistoryEntry)
	# Set outpost data
	var OutpostData : Dictionary = {
		# Info
		"Name" : outpost,
		"Type" : "Outpost",
		"History" : History,
		"InitialPosition" : outpostinfo["InitialPosition"],
		"InitialShieldMaximum" : outpostinfo["InitialShieldMaximum"],
		# Generation variables
		"PreviousTroopsGenerationTick" : 0,
		"PreviousShieldGenerationTick" : 0,
		# Items variables
		"LaunchedSubmarines" : {},
		"LaunchedSubmarinesKeys" : [],
		# Totals variables
		"Visible" : Visible,
		"VisibleKeys" : VisibleKeys,
		"Troops" : {0 : {outpost : outpostinfo["InitialTroops"]}},
		"TroopsKeys" : [0],
		"Shield" : {0 : {outpost : 0}},
		"ShieldKeys" : [0],
		"TroopsNumberMultipliers" : {0 : {outpost : 1}},
		"TroopsNumberMultipliersKeys" : [0],
		"TroopsTicksMultipliers" : {0 : {outpost : 1}},
		"TroopsTicksMultipliersKeys" : [0],
		"ShieldMaximumMultipliers" : {0 : {outpost : 1}},
		"ShieldMaximumMultipliersKeys" : [0],
		"SonarRangeMultipliers" : {0 : {outpost : 1}},
		"SonarRangeMultipliersKeys" : [0],
		# Current variables
		"Players" : {0 : {outpost : outpostinfo["InitialPlayer"]}},
		"PlayersKeys" : [0],
		"Types" : {0 : {outpost : outpostinfo["InitialType"]}},
		"TypesKeys" : [0]
	}
	GameVariables.OutpostData[outpost] = OutpostData
	# Set player info
	if outpostinfo["InitialPlayer"] != "Dormant":
		var PlayerInfo : Dictionary = GameVariables.PlayersInfo[outpostinfo["InitialPlayer"]]
		# Set player info outpost troops
		set_data_troops(0, PlayerInfo, outpost, outpostinfo["InitialTroops"])
		# Set player info outpost charge
		if outpostinfo["InitialType"] == "Generator":
			set_data_charge(0, PlayerInfo, outpost, GameVariables.DefaultChargePerGenerator)

# Calculates the data for an submarine
func calculate_submarine_data(launchorder : Dictionary) -> void:
	# Get inital variables
	var InitialTick : int = launchorder["Tick"]
	var InitialOutpost : String = launchorder["InitialOutpost"]
	var InitialOutpostData : Dictionary = GameVariables.OutpostData[InitialOutpost]
	# Get submarine name
	var Submarine : String = GameState.get_new_submarine_name(InitialTick, InitialOutpost)
	# Get submarine visible
	var Visible : Dictionary
	var VisibleKeys : Array
	var VisibleTotal : int
	if launchorder["Player"] == GameVariables.Player:
		VariableFunctions.set_keys_list(Visible, VisibleKeys, launchorder["Tick"], Submarine, 1)
		VisibleTotal = 1
	# Get submarine history
	var HistoryEntry : Dictionary = {
		"Tick" : InitialTick,
		# Status variables
		"Position" : InitialOutpostData["InitialPosition"],
		# Totals variables
		"VisibleTotal" : VisibleTotal,
		"TroopsTotal" : launchorder["InitialTroops"],
		"SpeedMultiplierTotal" : 1,
		# Current variables
		"CurrentPlayer" : launchorder["Player"],
		"CurrentTarget" : launchorder["InitialTarget"],
		"CurrentGift" : false
	}
	var History : Array = [HistoryEntry]
	# Set submarine data
	var Data : Dictionary = {
		# Info
		"Name" : Submarine,
		"Type" : "Submarine",
		"History" : History,
		"InitialOutpost" : InitialOutpost,
		# Launch order variables
		"LaunchOrderTick" : launchorder["Tick"],
		"LaunchOrderId" : launchorder["OrderId"],
		# Items variables
		"CombatSubmarines" : {},
		"CombatSubmarinesKeys" : [],
		# Totals variables
		"Visible" : Visible,
		"VisibleKeys" : VisibleKeys,
		"Troops" : {InitialTick : {Submarine : launchorder["InitialTroops"]}},
		"TroopsKeys" : [InitialTick],
		"SpeedMultipliers" : {InitialTick : {Submarine : 1}},
		"SpeedMultipliersKeys" : [InitialTick],
		# Current variables
		"Players" : {InitialTick : {Submarine : launchorder["Player"]}},
		"PlayersKeys" : [InitialTick],
		"Targets" : {InitialTick : {launchorder["OrderId"] : launchorder["InitialTarget"]}},
		"TargetsKeys" : [InitialTick],
		"Gifts" : {InitialTick : {launchorder["OrderId"] : false}},
		"GiftsKeys" : [InitialTick]
	}
	# Set submarine data
	GameVariables.SubmarineData[Submarine] = Data
	# Add submarine to active submarines
	GameVariables.ActiveSubmarines.append(Submarine)
	# Adds submarine to initial outpost launched submarines
	DataFunctions.set_data_launched_submarine(InitialTick, InitialOutpostData, Submarine)
	# Validates launch order
	OrderFunctions.calculate_launch_order_invalid(launchorder)
	# Calculate submarine effect
	CombatFunctions.calculate_submarine_effect(Submarine)
	# Update game scene
	if GameVariables.GameNode != null:
		GameVariables.GameNode.set_submarine_data(Submarine)
		GameVariables.GameNode.get_node("Outposts/" + InitialOutpost).update_tick()
		if GameState.FocusNextSubmarine:
			GameVariables.GameNode.focus_submarine(Submarine)
			GameState.FocusNextSubmarine = false

# Get variables

# Returns the data for passed item
func get_item_data(item : String) -> Dictionary:
	if GameVariables.OutpostData.has(item):
		return GameVariables.OutpostData[item]
	if GameVariables.SubmarineData.has(item):
		return GameVariables.SubmarineData[item]
	# Failure case
	return {}

# Returns previous present tick for passed source
func get_data_total_previous_source_tick(tick : int, data : Dictionary, list : String, listkeys : String, source : String) -> int:
	var InvertedListKeys : Array = data[listkeys].duplicate(true)
	InvertedListKeys.invert()
	for sourcetick in InvertedListKeys:
		if sourcetick <= tick:
			if data[list][sourcetick].has(source):
				return sourcetick
	# Failure case
	return -1

# Returns the total value at passed tick for passed source
func get_data_total_source_value(tick : int, data : Dictionary, list : String, source : String) -> int:
	var TotalValue : int
	if data[list].has(tick) and data[list][tick].has(source):
		TotalValue = data[list][tick][source]
	return TotalValue

# Returns the total value at passed tick
func get_data_total_value(tick : int, data : Dictionary, list : String) -> int:
	var TotalValue : int
	if data[list].has(tick):
		for source in data[list][tick]:
			TotalValue += data[list][tick][source]
	return TotalValue

# Returns the current position for passed item
func get_item_position(tick : int, item : String) -> Vector2:
	var Data : Dictionary = get_item_data(item)
	if Data["Type"] == "Outpost":
		return Data["InitialPosition"]
	else:
		var History : Array = Data["History"]
		var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(tick, History)
		return HistoryEntry["Position"]

# Sort functions

# TODO: reimplement
func sort_data_troops(a : Dictionary, b : Dictionary) -> bool:
	return GameVariables.SubmarineData[a]["History"][-1]["TroopsTotal"] > GameVariables.SubmarineData[b]["History"][-1]["TroopsTotal"]

# Variable functions

# Sets keys list total entry in data and calculates new total for history entries
func set_data_total(tick : int, data : Dictionary, list : String, listkeys : String, total : String, source : String, value : int, updatehistorytotal : bool = true) -> void:
	var PreviousSourceValue : int = get_data_total_source_value(tick, data, list, source)
	VariableFunctions.set_keys_list(data[list], data[listkeys], tick, source, value)
	if updatehistorytotal:
		var CurrentSourceValue : int = get_data_total_source_value(tick, data, list, source)
		HistoryFunctions.update_history_total(tick, data, total, PreviousSourceValue, CurrentSourceValue)

# Removes keys list total entry in data and calculates new total for history entries
func remove_data_total(tick : int, data : Dictionary, list : String, listkeys : String, total : String, source : String, updatehistorytotal : bool = true) -> void:
	if data[list].has(tick) and data[list][tick].has(source):
		var PreviousSourceValue : int = get_data_total_source_value(tick, data, list, source)
		VariableFunctions.remove_keys_list(data[list], data[listkeys], tick, source)
		if updatehistorytotal:
			var CurrentSourceValue : int = get_data_total_source_value(tick, data, list, source)
			HistoryFunctions.update_history_total(tick, data, total, PreviousSourceValue, CurrentSourceValue)

# Sets keys list current item entry in data and calculates new current item for history entries
func set_data_current_item(tick : int, data : Dictionary, list : String, listkeys : String, current : String, source : String, value) -> void:
	VariableFunctions.set_keys_list(data[list], data[listkeys], tick, source, value)
	HistoryFunctions.update_history_current_item(tick, data, list, listkeys, current)

# Removes keys list current item entry in data and calculates new current item for history entries
func remove_data_current_item(tick : int, data : Dictionary, list : String, listkeys : String, current : String, source : String) -> void:
	if data[list].has(tick) and data[list][tick].has(source):
		VariableFunctions.remove_keys_list(data[list], data[listkeys], tick, source)
		HistoryFunctions.update_history_current_item(tick, data, list, listkeys, current)

# Sets keys list current order entry in data and calculates new current order for history entries
func set_data_current_order(tick : int, data : Dictionary, list : String, listkeys : String, current : String, order : Dictionary, value) -> void:
	VariableFunctions.set_keys_list(data[list], data[listkeys], order["Tick"], order["OrderId"], value)
	HistoryFunctions.update_history_current_order(tick, data, list, listkeys, current)

# Removes keys list current order entry in data and calculates new current order for history entries
func remove_data_current_order(tick : int, data : Dictionary, list : String, listkeys : String, current : String, order : Dictionary) -> void:
	if data[list].has(order["Tick"]) and data[list][tick].has(order["OrderId"]):
		VariableFunctions.remove_keys_list(data[list], data[listkeys], order["Tick"], order["OrderId"])
		HistoryFunctions.update_history_current_order(tick, data, list, listkeys, current)

# Removes list entries in the future and calculates history variables
func remove_data_future_list_entries(tick : int, data : Dictionary, listtype : String, list : String, listkeys : String, historyvariable : String) -> void:
	var InvertedListKeys : Array = data[listkeys].duplicate(true)
	InvertedListKeys.invert()
	for itemtick in InvertedListKeys:
		if tick <= itemtick:
			var Start : bool = itemtick == data["History"][0]["Tick"]
			for item in data[list][itemtick]:
				if (Start and item == data["Name"]) == false:
					match listtype:
						"Total":
							remove_data_total(itemtick, data, list, listkeys, historyvariable, item)
						"CurrentItem":
							remove_data_current_item(itemtick, data, list, listkeys, historyvariable, item)
		else:
			break

# Set data items

# Sets data launched submarine
func set_data_launched_submarine(tick : int, data : Dictionary, submarine : String) -> void:
	VariableFunctions.set_keys_list(data["LaunchedSubmarines"], data["LaunchedSubmarinesKeys"], tick, submarine)

# Removes data launched submarine
func remove_data_launched_submarine(tick : int, data : Dictionary, submarine : String) -> void:
	VariableFunctions.remove_keys_list(data["LaunchedSubmarines"], data["LaunchedSubmarinesKeys"], tick, submarine)

# Sets data combat submarine
func set_data_combat_submarine(tick : int, data : Dictionary, submarine : String) -> void:
	VariableFunctions.set_keys_list(data["CombatSubmarines"], data["CombatSubmarinesKeys"], tick, submarine)

# Removes data combat submarine
func remove_data_combat_submarine(tick : int, data : Dictionary, submarine : String) -> void:
	VariableFunctions.remove_keys_list(data["CombatSubmarines"], data["CombatSubmarinesKeys"], tick, submarine)

# Set data totals

# Sets data visible
func set_data_visible(tick : int, data : Dictionary, source : String, value : int) -> void:
	var UpdateHistoryTotal : int = tick <= GameVariables.GlobalTick or value == -1
	set_data_total(tick, data, "Visible", "VisibleKeys", "VisibleTotal", source, value, UpdateHistoryTotal)

# Removes data visible
func remove_data_visible(tick : int, data : Dictionary, source : String) -> void:
	remove_data_total(tick, data, "Visible", "VisibleKeys", "VisibleTotal", source)

# Sets data troops
func set_data_troops(tick : int, data : Dictionary, source : String, value : int) -> void:
	set_data_total(tick, data, "Troops", "TroopsKeys", "TroopsTotal", source, value)

# Removes data troops
func remove_data_troops(tick : int, data : Dictionary, source : String) -> void:
	remove_data_total(tick, data, "Troops", "TroopsKeys", "TroopsTotal", source)

# Sets data shield
func set_data_shield(tick : int, data : Dictionary, source : String, value : int) -> void:
	set_data_total(tick, data, "Shield", "ShieldKeys", "ShieldTotal", source, value)

# Removes data shield
func remove_data_shield(tick : int, data : Dictionary, source : String) -> void:
	remove_data_total(tick, data, "Shield", "ShieldKeys", "ShieldTotal", source)

# Sets data charge
func set_data_charge(tick : int, data : Dictionary, source : String, value : int) -> void:
	set_data_total(tick, data, "Charge", "ChargeKeys", "ChargeTotal", source, value)

# Removes data charge
func remove_data_charge(tick : int, data : Dictionary, source : String) -> void:
	remove_data_total(tick, data, "Charge", "ChargeKeys", "ChargeTotal", source)

# Set data currents

# Sets data player
func set_data_player(tick : int, data : Dictionary, source : String, value : String) -> void:
	set_data_current_item(tick, data, "Players", "PlayersKeys", "CurrentPlayer", source, value)

# Removes data player
func remove_data_player(tick : int, data : Dictionary, source : String) -> void:
	remove_data_current_item(tick, data, "Players", "PlayersKeys", "CurrentPlayer", source)

# Sets data gift
func set_data_gift(tick : int, data : Dictionary, order : Dictionary, value : bool) -> void:
	set_data_current_order(tick, data, "Gifts", "GiftsKeys", "CurrentGift", order, value)

# Removes data gift
func remove_data_gift(tick : int, data : Dictionary, order : Dictionary) -> void:
	remove_data_current_order(tick, data, "Gifts", "GiftsKeys", "CurrentGift", order)

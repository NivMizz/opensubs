extends Node

# Sets the list entry for source at source tick and updates list keys
func set_keys_list(list : Dictionary, listkeys : Array, sourcetick : int, source, value = null) -> void:
	if list.has(sourcetick) == false:
		# Sets source tick in list as empty
		if value == null:
			list[sourcetick] = []
		else:
			list[sourcetick] = {}
		# Updates list keys
		listkeys.append(sourcetick)
		listkeys.sort()
	# Sets source tick entry in list
	if value == null:
		list[sourcetick].append(source)
	else:
		list[sourcetick][source] = value

# Removes the list entry for source at source tick and updates list keys
func remove_keys_list(list : Dictionary, listkeys : Array, sourcetick : int, source) -> void:
	if list.has(sourcetick):
		# Removes source tick entry from list
		list[sourcetick].erase(source)
		if list[sourcetick].empty():
			# Removes source tick from list
			list.erase(sourcetick)
			# Updates list keys
			listkeys.erase(sourcetick)
			listkeys.sort()

# Converts a number of seconds to a HH:MM:SS or HHh MMm SSs format
func get_time_text(time : int, letters : bool = false, accuracy : int = 3) -> String:
	var RemainingAccurary : int = accuracy
	var TimeText : String
	if RemainingAccurary > 0:
		# Gets text for hours
		var Hours : int = time / 3600
		if Hours > 0 or RemainingAccurary != accuracy:
			RemainingAccurary -= 1
		if Hours > 0:
			var UnitSeparator : String
			if accuracy >= 3:
				UnitSeparator = ":"
			if letters:
				UnitSeparator = "h"
			TimeText = str(Hours) + UnitSeparator
	if RemainingAccurary > 0:
		# Gets text for minutes
		var Minutes : int = (int(time) / 60) % 60
		if Minutes > 0:
			var MinutesPrefix : String
			if letters:
				if RemainingAccurary != accuracy:
					MinutesPrefix += " "
			elif Minutes <= 9:
				MinutesPrefix += "0"
			var UnitSeparator : String
			if accuracy >= 2:
				UnitSeparator = ":"
			if letters:
				UnitSeparator = "m"
			TimeText += MinutesPrefix + str(Minutes) + UnitSeparator
		elif letters == false:
			TimeText += "00:"
		if Minutes > 0 or RemainingAccurary != accuracy:
			RemainingAccurary -= 1
	if RemainingAccurary > 0:
		# Gets text for seconds
		var Seconds : int = time % 60
		if Seconds > 0:
			var SecondsPrefix : String
			if letters:
				if RemainingAccurary != accuracy:
					SecondsPrefix += " "
			elif Seconds <= 9:
				SecondsPrefix += "0"
			var UnitSeparator : String
			if letters:
				UnitSeparator = "s"
			TimeText += SecondsPrefix + str(Seconds) + UnitSeparator
		elif letters == false:
			TimeText += "00"
		elif RemainingAccurary == accuracy:
			TimeText += "Now"
		if Seconds > 0 or RemainingAccurary != accuracy:
			RemainingAccurary -= 1
	return TimeText

# Returns the username's player
func get_username_player(username : String) -> String:
	for player in GameVariables.Players:
		if GameVariables.Players[player]["Username"] == username:
			return player
	# Failure case
	print("\"" + username + "\" is not in player list")
	return ""

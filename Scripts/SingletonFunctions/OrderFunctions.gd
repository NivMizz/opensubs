extends Node

# Order functions

# Sets and calculates an order
func set_order(ordertick : int, orderid : int, order : Dictionary) -> void:
	# Set order variables
	order["Tick"] = ordertick
	order["OrderId"] = orderid
	# Set order in orders
	if GameVariables.InitialCalculation == false:
		VariableFunctions.set_keys_list(GameVariables.Orders, GameVariables.OrdersKeys, order["Tick"], order["OrderId"], order)
	# Calculates order
	calculate_order(order)
	# Set game scene
	if GameVariables.GameNode != null:
		GameVariables.GameNode.get_node("Interface/Panels/Time").set_queued_order(order)
		# TODO: remove
		GameVariables.GameNode.update_tick()

# Removes order
func remove_order(order : Dictionary):
	# Cancels order effect
	cancel_order(order)
	# Removes order from orders
	VariableFunctions.remove_keys_list(GameVariables.Orders, GameVariables.OrdersKeys, order["Tick"], order["OrderId"])
	# Remove order from all order lists
	remove_order_in_all_order_lists(order)
	# Removes order from submarine data
	if order["Type"] == "Gift":
		var LaunchOrder : Dictionary = GameVariables.Orders[order["LaunchOrderTick"]][order["LaunchOrderId"]]
		var Submarine : String = get_submarine_from_launch_order(LaunchOrder)
		var SubmarineData : Dictionary = GameVariables.SubmarineData[Submarine]
		VariableFunctions.remove_keys_list(SubmarineData["GiftOrders"], SubmarineData["GiftOrdersKeys"], order["Tick"], order["OrderId"])
	# Set game scene
	if GameVariables.GameNode != null:
		GameVariables.GameNode.get_node("Interface/Panels/Time").remove_queued_order(order)
		# TODO: remove
		GameVariables.GameNode.update_tick()

# Updates the current game state based on all revealed orders given
func set_revealed_orders(globaltick : int, revealedorders : Dictionary) -> void:
	if GameVariables.Orders.has(globaltick) == false:
		GameVariables.Orders[globaltick] = {}
	for orderid in revealedorders:
		var Order : Dictionary = GameVariables.Orders[globaltick][orderid]
		OrderFunctions.calculate_order(Order)

# Calculates order effect
func calculate_order(order : Dictionary) -> void:
	match order["Type"]:
		"Launch":
			# TODO: move to order functions
			DataFunctions.calculate_submarine_data(order)
		"Gift":
			calculate_gift_order(order)

# Cancels order effect
func cancel_order(order : Dictionary) -> void:
	orders_reasons_set_order(GameVariables.InvalidOrders, order, ["Cancelled"])
	match order["Type"]:
		"Launch":
			cancel_launch_order(order)
		"Gift":
			cancel_gift_order(order)

# Cancels the effect of a launch order
func cancel_launch_order(launchorder : Dictionary) -> void:
	print("Cancelling submarine launch")
	# Get submarne data
	var Submarine : String = get_submarine_from_launch_order(launchorder)
	var Data : Dictionary = GameVariables.SubmarineData[Submarine]
	var History : Array = Data["History"].duplicate(true)
	var StartTick : int = History[0]["Tick"]
	# Get initial outpost data
	var InitialOutpost : String = Data["InitialOutpost"]
	var InitialOutpostData : Dictionary = GameVariables.OutpostData[InitialOutpost]
	# Updates game scene submarine
	GameVariables.GameNode.cancel_submarine(Submarine)
	# Erase submarine from initial outpost submarines
	DataFunctions.remove_data_launched_submarine(StartTick, InitialOutpostData, Submarine)
	# Calculates submarine effect, as cancelled submarine
	CombatFunctions.calculate_submarine_effect(Submarine)
	# Removes submarine from submarine data
	GameVariables.SubmarineData.erase(Submarine)
	# Updates game scene initial outpost
	GameVariables.GameNode.override_outpost_temporary_troops(InitialOutpost)

# Calculates the effect of a gift order
func calculate_gift_order(giftorder : Dictionary) -> void:
	print("Calculating submarine gift")
	# Get submarine data
	var LaunchOrder : Dictionary = GameVariables.Orders[giftorder["LaunchOrderTick"]][giftorder["LaunchOrderId"]]
	var Submarine : String = get_submarine_from_launch_order(LaunchOrder)
	var Data : Dictionary = GameVariables.SubmarineData[Submarine]
	# Validates gift order
	OrderFunctions.calculate_gift_order_invalid(giftorder)
	# Sets gift order history effect
	if GameVariables.InitialCalculation == false:
		# Calculates all other future gift orders invalid
		for order in get_all_submarine_orders(Submarine, "Gift"):
			if order["Tick"] >= giftorder["Tick"]:
				if order != giftorder:
					OrderFunctions.calculate_order_invalid(order)
		# Sets submarine gift
		var GiftEffectTick : int = giftorder["Tick"] + GameVariables.GiftWaitTicks
		DataFunctions.set_data_gift(GiftEffectTick, Data, giftorder, true)
		CombatFunctions.calculate_submarine_effect(Submarine, GiftEffectTick)

# Cancels the effect of a gift order
func cancel_gift_order(giftorder : Dictionary) -> void:
	print("Cancelling submarine gift")
	# Get submarine data
	var LaunchOrder : Dictionary = GameVariables.Orders[giftorder["LaunchOrderTick"]][giftorder["LaunchOrderId"]]
	var Submarine : String = get_submarine_from_launch_order(LaunchOrder)
	var Data : Dictionary = GameVariables.SubmarineData[Submarine]
	var History : Array = Data["History"]
	# Sets gift order history effect
	if GameVariables.InitialCalculation == false:
		# Calculates all other future gift orders invalid
		for order in get_all_submarine_orders(Submarine, "Gift"):
			if order != giftorder:
				if order["Tick"] >= giftorder["Tick"]:
					OrderFunctions.calculate_order_invalid(order)
		# Removes submarine gift
		var GiftEffectTick : int = giftorder["Tick"] + GameVariables.GiftWaitTicks
		HistoryFunctions.remove_history_gift(GiftEffectTick, History, giftorder)
		CombatFunctions.calculate_submarine_effect(Submarine, GiftEffectTick)

# TODO: rework
# Returns all orders for the passed submarine
func get_all_submarine_orders(submarine : String, type : String = "") -> Array:
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var SubmarineOrders : Array
	if type == "" or type == "Gift":
		for ordertick in Data["Gifts"]:
			for orderid in Data["Gifts"][ordertick]:
				var GiftOrder : Dictionary = GameVariables.Orders[ordertick][orderid]
				if GiftOrder["Type"] == "Gift":
					SubmarineOrders.append(GiftOrder)
	if type == "" or type == "Launch":
		var LaunchOrder : Dictionary = GameVariables.Orders[Data["LaunchOrderTick"]][Data["LaunchOrderId"]]
		SubmarineOrders.append(LaunchOrder)
	return SubmarineOrders

# Returns the submarine data for the passed launch order 
func get_submarine_from_launch_order(launchorder : Dictionary) -> String:
	for submarine in GameVariables.SubmarineData:
		var Data : Dictionary = GameVariables.SubmarineData[submarine]
		if Data["LaunchOrderId"] == launchorder["OrderId"]:
			return submarine
	# Failure case
	return ""

# Returns a the first order awaing effect of passed orders
func get_awaiting_effect_order(orders : Dictionary, orderskeys : Array, type : String) -> Dictionary:
	for ordertick in orderskeys:
		for orderid in orders[ordertick]:
			var Order : Dictionary = GameVariables.Orders[ordertick][orderid]
			if Order["Type"] == type:
				if get_order_awaiting_effect(Order):
					return Order
	# Failure case
	return {}

# Returns true if the order has been started but effect has not yet been applied
func get_order_awaiting_effect(order : Dictionary) -> bool:
	var WaitTicks : int
	match order["Type"]:
		"Launch":
			WaitTicks = GameVariables.LaunchWaitTicks
		"Gift":
			WaitTicks = GameVariables.GiftWaitTicks
	return order["Tick"] <= GameVariables.VisibleTick and order["Tick"] + WaitTicks >= GameVariables.VisibleTick

# Sets launch order troops
func set_launch_order_troops(launchorder : Dictionary, troops : int) -> void:
	var Submarine : String = get_submarine_from_launch_order(launchorder)
	var InitialOutpost : String = launchorder["InitialOutpost"]
	var Data : Dictionary = GameVariables.SubmarineData[Submarine]
	# Set order initial troops
	launchorder["InitialTroops"] = troops
	# Validates launch order
	calculate_launch_order_invalid(launchorder)
	# Sets submarine data
	DataFunctions.set_data_troops(launchorder["Tick"], Data, Submarine, troops)
	# Calculates submarine's effect
	CombatFunctions.calculate_submarine_effect(Submarine)
	# Update submarine in game
	GameVariables.GameNode.override_submarine_temporary_troops(Submarine)
	# Update initial outpost in game
	GameVariables.GameNode.override_outpost_temporary_troops(InitialOutpost)
	# Set game scene
	if GameVariables.GameNode != null:
		# TODO: remove
		GameVariables.GameNode.update_tick()

# Returns when an order starts
func get_order_time(order : Dictionary) -> int:
	var StartTime : int = GameVariables.StartTime
	if StartTime == -1:
		StartTime = OS.get_unix_time()
	var OrderTime : int = max(
		StartTime + (order["Tick"] * GameVariables.TickLength),
		order["Time"]
	)
	return OrderTime

# Return the time until an order starts
func get_time_to_order(order : Dictionary) -> int:
	var OrderTime : int = get_order_time(order)
	return OrderTime - OS.get_unix_time()

# Returns the time until a submarine order effect is applied
func get_submarine_time_to_order_effect(order : Dictionary) -> int:
	var WaitTicks : int
	match order["Type"]:
		"Launch":
			WaitTicks = GameVariables.LaunchWaitTicks
		"Gift":
			WaitTicks = GameVariables.GiftWaitTicks
	var TimeToEffect : int
	if GameVariables.VisibleTick == GameVariables.GlobalTick:
		var OrderTime : int = get_order_time(order)
		var EffectTime : int = OrderTime + (WaitTicks * GameVariables.TickLength)
		TimeToEffect = EffectTime - OS.get_unix_time()
	else:
		TimeToEffect = ((order["Tick"] + WaitTicks) - GameVariables.VisibleTick) * GameVariables.TickLength
	return int(max(TimeToEffect, 0))

# Returns order reasons
func get_order_reasons(ordersreasons : Dictionary, order : Dictionary) -> Array:
	if ordersreasons.has(order["Tick"]) and ordersreasons[order["Tick"]].has(order["OrderId"]):
		return ordersreasons[order["Tick"]][order["OrderId"]]
	# Failure case
	return []

# Sets an order in order reasons
func orders_reasons_set_order(ordersreasons : Dictionary, order : Dictionary, reasons : Array) -> void:
	if ordersreasons.has(order["Tick"]) == false:
		ordersreasons[order["Tick"]] = {}
	ordersreasons[order["Tick"]][order["OrderId"]] = reasons

# Sets an order from order reasons
func orders_reasons_remove_order(ordersreasons : Dictionary, order : Dictionary) -> void:
	if ordersreasons.has(order["Tick"]):
		ordersreasons[order["Tick"]].erase(order["OrderId"])
		if ordersreasons[order["Tick"]].empty():
			ordersreasons.erase(order["Tick"])

# Removes an order from all order lists
func remove_order_in_all_order_lists(order : Dictionary) -> void:
	orders_reasons_remove_order(GameVariables.InvalidOrders, order)
	orders_reasons_remove_order(GameVariables.WarningOrders, order)
	# Update queued order buttons
	if GameVariables.GameNode != null:
		GameVariables.GameNode.get_node("Interface/Panels/Time").update_queued_order_status(order)

# Sets order list order to passed reasons or removes if reasons empty
func orders_reasons_update_order(ordersreasons : Dictionary, order : Dictionary, reasons : Array) -> void:
	if reasons.empty() == false:
		orders_reasons_set_order(ordersreasons, order, reasons)
	else:
		orders_reasons_remove_order(ordersreasons, order)
	# Update queued order buttons
	if GameVariables.GameNode != null:
		GameVariables.GameNode.get_node("Interface/Panels/Time").update_queued_order_status(order)

# Calculates if an order if invalid

# Checks if an order is invalid
func get_order_invalid(order : Dictionary) -> bool:
	if GameVariables.InvalidOrders.has(order["Tick"]):
		if GameVariables.InvalidOrders[order["Tick"]].has(order["OrderId"]):
			return true
	return false

# Checks if an order is warning
func get_order_warning(order : Dictionary) -> bool:
	if GameVariables.WarningOrders.has(order["Tick"]):
		if GameVariables.WarningOrders[order["Tick"]].has(order["OrderId"]):
			return true
	return false

# Calculates if an order is invalid
func calculate_order_invalid(order : Dictionary) -> void:
	match order["Type"]:
		"Launch":
			calculate_launch_order_invalid(order)
		"Gift":
			calculate_gift_order_invalid(order)

# Calculates if an order is warning
func calculate_order_warning(order : Dictionary) -> void:
	match order["Type"]:
		"Gift":
			calculate_gift_order_warning(order)

# Calculates if a submarine launch order is invalid
func calculate_launch_order_invalid(launchorder : Dictionary) -> void:
	# Get submarine data
	var Submarine : String = get_submarine_from_launch_order(launchorder)
	var SubmarineData : Dictionary = GameVariables.SubmarineData[Submarine]
	var SubmarineHistory : Array = SubmarineData["History"]
	# Check that submarine is not cancelled
	if get_order_reasons(GameVariables.InvalidOrders, launchorder).has("Cancelled") == false:
		# Get initial outpost data
		var InitialOutpost : String = SubmarineData["InitialOutpost"]
		var InitialOutpostData : Dictionary= GameVariables.OutpostData[InitialOutpost]
		var InitialOutpostHistory : Array = InitialOutpostData["History"]
		var InitialOutpostHistoryEntry : Dictionary = InitialOutpostHistory[SubmarineHistory[0]["Tick"] + GameVariables.LaunchWaitTicks - 1]
		# Get total troops at outpost
		var AvailableTroops : int = InitialOutpostHistoryEntry["TroopsTotal"] - DataFunctions.get_data_total_source_value(launchorder["Tick"], InitialOutpostData, "Troops", Submarine)
		# Get reasons to set order as invalid
		var InvalidReasons : Array
		# Checks if there are too few troops
		if launchorder["InitialTroops"] <= 0:
			InvalidReasons.append("AbsentTroops")
		# Checks if there are too many troops
		elif launchorder["InitialTroops"] > AvailableTroops:
			InvalidReasons.append("ExcessTroops")
		# Checks if the outpost is captured before submarine is launched
		var OutpostCaptured : bool = false
		for i in GameVariables.LaunchWaitTicks:
			if InitialOutpostHistory[SubmarineHistory[0]["Tick"] + i]["CurrentPlayer"] != SubmarineHistory[0]["CurrentPlayer"]:
				OutpostCaptured = true
				break
		if OutpostCaptured:
			InvalidReasons.append("OutpostCaptured")
		# Sets invalid order
		orders_reasons_update_order(GameVariables.InvalidOrders, launchorder, InvalidReasons)
		# Updates game scene
		if GameVariables.GameNode != null:
			# Updates submarine toasts
			var SubmarineNode : Node = GameVariables.GameNode.get_node_or_null("Submarines/" + Submarine)
			if SubmarineNode != null:
				SubmarineNode.update_tick()
				GameVariables.GameNode.update_tick_panels()
				GameVariables.GameNode.set_submarine_toasts(Submarine)

# Calculates if a submarine gift order is invalid
func calculate_gift_order_invalid(giftorder : Dictionary) -> void:
	# Get submarine data
	var LaunchOrder : Dictionary = GameVariables.Orders[giftorder["LaunchOrderTick"]][giftorder["LaunchOrderId"]]
	var Submarine : String = get_submarine_from_launch_order(LaunchOrder)
	var Data : Dictionary = GameVariables.SubmarineData[Submarine]
	var History : Array = Data["History"]
	# Get reasons to invalidate order
	var InvalidReasons : Array
	# Checks if the submarine has yet to launch
	if giftorder["Tick"] < History[0]["Tick"]:
		InvalidReasons.append("BeforeLaunch")
	# Checks if the submarine has already arrived
	elif giftorder["Tick"] > History[-1]["Tick"]:
		InvalidReasons.append("AfterArrival")
	# Checks if the submarine is invalid
	if get_order_invalid(LaunchOrder):
		InvalidReasons.append("InvalidSubmarine")
	# Checks if the submarine has already gifted
	for secondarygiftorder in get_all_submarine_orders(Submarine, "Gift"):
		if get_order_invalid(secondarygiftorder) == false:
			if secondarygiftorder != giftorder:
				InvalidReasons.append("AlreadyGift")
			break
	# Sets invalid order
	orders_reasons_update_order(GameVariables.InvalidOrders, giftorder, InvalidReasons)

# Calculates if a submarine gift order is warning
func calculate_gift_order_warning(giftorder : Dictionary) -> void:
	# Get submarine data
	var LaunchOrder : Dictionary = GameVariables.Orders[giftorder["LaunchOrderTick"]][giftorder["LaunchOrderId"]]
	var Submarine : String = get_submarine_from_launch_order(LaunchOrder)
	var Data : Dictionary = GameVariables.SubmarineData[Submarine]
	var History : Array = Data["History"]
	# Get reasons to set order as warning
	var WarningReasons : Array
	if giftorder["Tick"] + GameVariables.GiftWaitTicks > History[-1]["Tick"]:
		WarningReasons.append("TooLate")
	# Sets warning order
	orders_reasons_update_order(GameVariables.WarningOrders, giftorder, WarningReasons)

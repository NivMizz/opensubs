extends Node

# Updates obscured item visible for passed sonar item at passed tick
func update_obscured_item_sonar_item_visible(tick : int, obscureditem : String, sonaritem : String, obscureditemprevioussonaritemtick : int = -1) -> int:
	# Get data
	var ObscuredItemData : Dictionary = DataFunctions.get_item_data(obscureditem)
	var SonarItemData : Dictionary = DataFunctions.get_item_data(sonaritem)
	# Get obscured item visible
	var ObscuredItemSonarItemValue : int = 0
	if get_sonar_item_status(tick, sonaritem):
		var SonarItemSonarRange : float = get_outpost_sonar_range(tick, sonaritem)
		var SonarItemPosition : Vector2 = DataFunctions.get_item_position(tick, sonaritem)
		var ObscuredItemPosition : Vector2 = DataFunctions.get_item_position(tick, obscureditem)
		var ObscuredItemWrappedPosition : Vector2 = MapFunctions.get_closest_wrapped_position(SonarItemPosition, ObscuredItemPosition)
		var ObscuredItemDistance : float = SonarItemPosition.distance_to(ObscuredItemWrappedPosition)
		ObscuredItemSonarItemValue = bool(ObscuredItemDistance <= SonarItemSonarRange)
	# Get previous obscured item sonar item tick
	var ObscuredItemPreviousSonarItemTick : int = obscureditemprevioussonaritemtick
	if obscureditemprevioussonaritemtick == -1:
		ObscuredItemPreviousSonarItemTick = DataFunctions.get_data_total_previous_source_tick(tick, ObscuredItemData, "Visible", "VisibleKeys", sonaritem)
	# Set oubscured item visible
	if ObscuredItemSonarItemValue == 1:
		if ObscuredItemPreviousSonarItemTick == -1:
			# Set sonar item in obscured item visible as active
			DataFunctions.set_data_visible(tick, ObscuredItemData, sonaritem, 1)
			return tick
	else:
		if ObscuredItemPreviousSonarItemTick != -1:
			if ObscuredItemPreviousSonarItemTick != tick:
				# Set sonar item in obscured item visible as inactive
				var ObscuredItemPreviousSonarItemValue = DataFunctions.get_data_total_source_value(ObscuredItemPreviousSonarItemTick, ObscuredItemData, "Visible", sonaritem)
				if ObscuredItemPreviousSonarItemValue == 1:
					DataFunctions.set_data_visible(tick, ObscuredItemData, sonaritem, -1)
					return tick
			else:
				# Remove sonar item from obscured item visible
				DataFunctions.remove_data_visible(tick, ObscuredItemData, sonaritem)
				return tick
	# Return previous obscured item sonar item tick
	return ObscuredItemPreviousSonarItemTick

# Updates obscured item visible for all sonar outposts at passed tick
func update_item_visible(tick : int, obscureditem : String) -> void:
	for sonaroutpost in GameVariables.OutpostData:
		if get_sonar_item_status(tick, sonaroutpost):
			update_obscured_item_sonar_item_visible(tick, obscureditem, sonaroutpost)

# Updates all items visible for sonar item value
func update_item_sonar(tick : int, sonaritem : String) -> void:
	# Update sonar outpost sonar
	if get_sonar_item_status(tick, sonaritem):
		# Update all outposts for sonar outpost
		for obscuredoutpost in GameVariables.OutpostData:
			if obscuredoutpost != sonaritem:
				# Set obscured outpost visible
				update_obscured_item_sonar_item_visible(tick, obscuredoutpost, sonaritem)
		# Update all submarine for sonar outpost
		for obscuredsubmarine in GameVariables.SubmarineData:
			if obscuredsubmarine != sonaritem:
				# Get obscured submarine data
				var ObscuredSubmarineData : Dictionary = GameVariables.SubmarineData[obscuredsubmarine]
				var ObscuredSubmarineHistory : Array = ObscuredSubmarineData["History"]
				var ObscuredSubmarineHistoryIndex : int = HistoryFunctions.get_history_index(tick, ObscuredSubmarineData["History"])
				# Set obscured submarine visible
				var PreviousObscuredItemSonarItemTick : int = DataFunctions.get_data_total_previous_source_tick(tick, ObscuredSubmarineData, "Visible", "VisibleKeys", sonaritem)
				for i in len(ObscuredSubmarineHistory) - ObscuredSubmarineHistoryIndex:
					var CurrentTick : int = tick + i
					PreviousObscuredItemSonarItemTick = update_obscured_item_sonar_item_visible(CurrentTick, obscuredsubmarine, sonaritem, PreviousObscuredItemSonarItemTick)

# Updates obscured item previous visible totals
func update_obscured_item_previous_sonar_total(previousglobaltick : int, obscureditem : String) -> void:
	# Get data
	var Data : Dictionary = DataFunctions.get_item_data(obscureditem)
	var History : Array = Data["History"]
	# Update obscured item sonar
	var UpdateTick : int = -1
	var AddVisible : int
	for i in len(History) - previousglobaltick:
		var CurrentTick : int = previousglobaltick + i
		var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(CurrentTick, History)
		if CurrentTick <= GameVariables.GlobalTick:
			AddVisible += DataFunctions.get_data_total_value(CurrentTick, Data, "Visible")
			if UpdateTick == -1 and AddVisible != 0:
				UpdateTick = CurrentTick
		HistoryEntry["VisibleTotal"] += AddVisible
	# Update obscured item effect
	if UpdateTick != -1:
		if Data["Type"] == "Submarine":
			CombatFunctions.calculate_submarine_effect(obscureditem, UpdateTick)

# Returns sonar item sonar status
func get_sonar_item_status(tick : int, sonaritem : String) -> bool:
	var Data : Dictionary = DataFunctions.get_item_data(sonaritem)
	var History : Array =  Data["History"]
	return (
		History[min(tick, GameVariables.GlobalTick)]["CurrentPlayer"] == GameVariables.Player and
		History[tick]["CurrentPlayer"] == GameVariables.Player
	)

# Returns an outpost's sonar range
func get_outpost_sonar_range(tick : int, outpost : String) -> float:
	var Data : Dictionary = GameVariables.OutpostData[outpost]
	var History : Array =  Data["History"]
	return GameVariables.DefaultSonarRange * GameVariables.SonarRangeMultiplier * History[tick]["SonarRangeMultipliersTotal"]

extends Node

var PoissonDiscSampleLimit = 30
var CurrentPlayerPoint : Vector2

func calculate_map() -> Dictionary:
	seed(GameVariables.GameId)
	var TotalPlayers : int = (len(GameVariables.Players) - 1)
	# Get outpost names
	var Names : Array
	match GameVariables.OutpostNames:
		"Classics":
			var NamesList := preload("res://Names/Classics.gd").new()
			Names = NamesList.Names
		"Capitals":
			var NamesList := preload("res://Names/Capitals.gd").new()
			Names = NamesList.Names
	# Get outpost positions
	var OutpostPointsTotal : int = TotalPlayers * GameVariables.OutpostsPerPlayer
	var OutpostPoints : Array
	var Length : int = ceil(sqrt(OutpostPointsTotal)) * GameVariables.OutpostSpacing
	match GameVariables.MapType:
		"Grid":
			GameVariables.MapSize = Vector2(Length, Length)
			GameVariables.HalfMapSize = GameVariables.MapSize / 2
			OutpostPoints = calculate_grid_points(OutpostPointsTotal, GameVariables.MapSize, GameVariables.OutpostSpacing)
		"RandomSpread":
			Length *= 1.35
			GameVariables.MapSize = Vector2(Length, Length)
			GameVariables.HalfMapSize = GameVariables.MapSize / 2
			OutpostPoints = calculate_random_spread_points(OutpostPointsTotal, GameVariables.MapSize, GameVariables.OutpostSpacing)
	# Get player starting positions
	var PlayerSpacing : float = Length / ceil(sqrt(TotalPlayers))
	var PlayerPoints : Array = calculate_random_spread_points(TotalPlayers, GameVariables.MapSize, PlayerSpacing)
	# Get player outposts
	var UnassignedPlayers : Array = GameVariables.Players.keys()
	UnassignedPlayers.erase("Dormant")
	var PlayerOutposts : Dictionary
	for playerpoint in PlayerPoints:
		# Get list of outpost points sorted by distance to player point
		CurrentPlayerPoint = playerpoint
		var SortedClosePoints : Array = OutpostPoints.duplicate(true)
		SortedClosePoints.sort_custom(self, "sort_player_outpost_distance")
		# Get player
		var Player : String = UnassignedPlayers[rand_range(0, len(UnassignedPlayers))]
		UnassignedPlayers.erase(Player)
		# Set starting outpost points
		var StartingPoints : Array = SortedClosePoints.slice(-5, -1)
		for startingpoint in StartingPoints:
			OutpostPoints.erase(startingpoint)
		PlayerOutposts[Player] = StartingPoints
	PlayerOutposts["Dormant"] = OutpostPoints
	# Set outposts info
	var OutpostInfo : Dictionary
	for player in PlayerOutposts:
		for i in len(PlayerOutposts[player]):
			# Get name
			var NameIndex : int = rand_range(0, len(Names))
			var Name : String = Names[NameIndex]
			Names.remove(NameIndex)
			# Get initial troops
			var Troops : int
			if GameVariables.Players[player]["Username"] == "Dormant":
				Troops = 0
			else:
				Troops = GameVariables.OutpostStartingTroops
			# Get initial type
			var Type : String
			if i % 2 == 0:
				Type = "Factory"
			else:
				Type = "Generator"
			# Set outpost info
			var Info : Dictionary = {
				"InitialPlayer" : player,
				"InitialPosition" : PlayerOutposts[player][i],
				"InitialType" : Type,
				"InitialTroops" : Troops,
				"InitialShieldMaximum" : [10, 20][rand_range(0, 1)]
			}
			OutpostInfo[Name] = Info
			# Check if there are no unused names
			if Names.empty():
				break
	print("Successfully generated " + str(len(OutpostInfo)) + " outposts")
	return OutpostInfo

func sort_player_outpost_distance(a : Vector2, b : Vector2) -> bool:
	var AWrappedPosition : Vector2 = get_closest_wrapped_position(CurrentPlayerPoint, a)
	var BWrappedPosition : Vector2 = get_closest_wrapped_position(CurrentPlayerPoint, b)
	return CurrentPlayerPoint.distance_to(AWrappedPosition) > CurrentPlayerPoint.distance_to(BWrappedPosition)

func calculate_grid_points(pointstotal : int, area : Vector2, spacing : int) -> Array:
	var Points : Array
	for x in int(area.x / spacing):
		for y in int(area.y / spacing):
			Points.append(Vector2(x * spacing, y * spacing) - GameVariables.MapSize / 2)
			if len(Points) >= pointstotal:
				break
	return Points

func calculate_random_spread_points(pointstotal : int, area : Vector2, spacing : int) -> Array:
	var Points : Array
	var SpawnPoints : Array
	# Create grid
	var Grid : Array
	var CellSize : int = spacing / sqrt(2)
	var GridSize : int = area.x / CellSize
	for x in GridSize:
		Grid.append([])
		for y in GridSize:
			Grid[x].append(-1)
	# Create first point
	var FirstPoint : Vector2 = Vector2(rand_range(0, GridSize * CellSize), rand_range(0, GridSize * CellSize))
	Points.append(FirstPoint - GameVariables.MapSize / 2)
	SpawnPoints.append(FirstPoint)
	var GridPosition : Vector2 = Vector2(int(SpawnPoints[0].x / CellSize), int(SpawnPoints[0].y / CellSize))
	Grid[GridPosition.x][GridPosition.y] = 0
	# Generate points
	while SpawnPoints.empty() == false and len(Points) < pointstotal: 
		var CandidateAccepted : bool = false
		# Get random spawn point
		var SpawnPointsIndex : int = rand_range(0, len(SpawnPoints) - 1)
		# Attempt to find new valid point adjacent to spawn point
		for i in PoissonDiscSampleLimit:
			# Get candidate point
			var Angle : float = rand_range(0, TAU)
			var Distance : Vector2 = Vector2(rand_range(spacing, spacing * 2), 0) 
			var CandidatePoint : Vector2 = SpawnPoints[SpawnPointsIndex] + Distance.rotated(Angle)
			var WrappedCandidatePoint : Vector2 = get_wrapped_position(CandidatePoint - GameVariables.MapSize / 2)
			var PositiveWrappedCandidatePoint : Vector2 = WrappedCandidatePoint + GameVariables.MapSize / 2
			# Get candidate point position on grid
			GridPosition = Vector2(posmod(PositiveWrappedCandidatePoint.x / CellSize, GridSize), posmod(PositiveWrappedCandidatePoint.y / CellSize, GridSize))
			# Check that grid point is empty
			if Grid[GridPosition.x][GridPosition.y] == -1:
				# Get all adjacent points
				var NearbyPoints : Array
				for x in 5:
					for y in 5:
						var NearbyPoint : Vector2 = Vector2(posmod(GridPosition.x - 2 + x, GridSize), posmod(GridPosition.y - 2 + y, GridSize))
						if Grid[NearbyPoint.x][NearbyPoint.y] != -1:
							NearbyPoints.append(Grid[NearbyPoint.x][NearbyPoint.y])
				# Check if nearby points are too close
				var PointValid : bool = true
				for checkpoint in NearbyPoints:
					var CheckPointWrapped : Vector2 = get_closest_wrapped_position(WrappedCandidatePoint, Points[checkpoint])
					if WrappedCandidatePoint.distance_to(CheckPointWrapped) < spacing:
						PointValid = false
						break
				if PointValid:
					# Add candidate points to points
					SpawnPoints.append(PositiveWrappedCandidatePoint)
					Points.append(WrappedCandidatePoint)
					Grid[GridPosition.x][GridPosition.y] = len(Points) - 1 
					CandidateAccepted = true
					# Check is points are full
					if len(Points) >= pointstotal:
						break
			else:
				continue
		# Remove point with too many surrounding other points
		if CandidateAccepted == false:
			SpawnPoints.remove(SpawnPointsIndex)
	return Points

# Get passed position wrapped over the map
func get_wrapped_position(position : Vector2) -> Vector2:
	var CameraPosition : Vector2
	if GameVariables.GameNode != null:
		CameraPosition = GameVariables.GameNode.get_node("Camera2D").global_position
	var GridPosition : Vector2 = (CameraPosition - position) / GameVariables.MapSize + Vector2(0.5, 0.5)
	return Vector2(floor(GridPosition.x), floor(GridPosition.y)) * GameVariables.MapSize + position

# Get closest position, accounting for map wrap
func get_closest_wrapped_position(position : Vector2, wrappedposition : Vector2) -> Vector2:
	var WrappedPositionDifference : Vector2 = Vector2(position.x - wrappedposition.x, position.y - wrappedposition.y)
	var GridOffset : Vector2 = Vector2(
		GameVariables.MapSize.x * (int(WrappedPositionDifference.x > GameVariables.HalfMapSize.x) - int(WrappedPositionDifference.x < -GameVariables.HalfMapSize.x)),
		GameVariables.MapSize.y * (int(WrappedPositionDifference.y > GameVariables.HalfMapSize.y) - int(WrappedPositionDifference.y < -GameVariables.HalfMapSize.y))
	)
	return wrappedposition + GridOffset

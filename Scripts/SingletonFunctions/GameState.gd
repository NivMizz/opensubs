extends Node

var FocusNextSubmarine : bool = false

# Game state functions

# Calculates the current game state based on all orders given
func calculate_game_state() -> void:
	# Set game variables
	GameVariables.Player = VariableFunctions.get_username_player(Settings.Username)
	GameVariables.VisibleTickMaximum = GameVariables.GlobalTick + Settings.VisibleTickMaximumAhead
	GameVariables.InitialCalculation = true
	# Calculate players info
	DataFunctions.calculate_players_info()
	# Generate map
	var CalculateMapStartTime : int = Time.get_ticks_msec()
	var OutpostsInfo : Dictionary = MapFunctions.calculate_map()
	print("Calculating map took " + str(Time.get_ticks_msec() - CalculateMapStartTime) + "ms")
	# Calculate outposts
	var CalculateOutpostsStartTime : int = Time.get_ticks_msec()
	for outpostinfo in OutpostsInfo:
		DataFunctions.calculate_outpost_data(outpostinfo, OutpostsInfo[outpostinfo])
	print("Calculating outpost data took " + str(Time.get_ticks_msec() - CalculateOutpostsStartTime) + "ms")
	# Calculate outpost visibility
	for outpost in GameVariables.OutpostData:
		# Get outpost data
		var OutpostData : Dictionary = GameVariables.OutpostData[outpost]
		var OutpostHistory : Array = OutpostData["History"]
		var StartOutpostHistoryEntry : Dictionary = OutpostHistory[0]
		# Update outpost sonar
		if StartOutpostHistoryEntry["CurrentPlayer"] == GameVariables.Player:
			SonarFunctions.update_item_sonar(0, outpost)
	# Calculate history
	var CalculateHistoryStartTime : int = Time.get_ticks_msec()
	GameVariables.OrdersKeys = GameVariables.Orders.keys()
	GameVariables.OrdersKeys.sort()
	for i in GameVariables.VisibleTickMaximum:
		# Sets new players info entry
		if i != 0:
			for player in GameVariables.PlayersInfo:
				var PlayerInfo : Dictionary = GameVariables.PlayersInfo[player]
				var PlayerInfoHistory : Array = PlayerInfo["History"]
				var NewPlayerInfoHistoryEntry : Dictionary = PlayerInfoHistory[-1].duplicate(true)
				NewPlayerInfoHistoryEntry["Tick"] = i
				PlayerInfoHistory.append(NewPlayerInfoHistoryEntry)
		# Calculates orders on tick
		if GameVariables.OrdersKeys.has(i):
			for orderid in GameVariables.Orders[i]:
				var Order : Dictionary = GameVariables.Orders[i][orderid]
				OrderFunctions.set_order(i, orderid, Order)
		# Calculates active submarines history entry
		calculate_active_submarines_history_entry(i)
		# Calculates all outposts military generation
		for outpost in GameVariables.OutpostData:
			GenerationFunctions.calculate_outpost_entry_military_generation(i, outpost)
	print("Calculating history took " + str(Time.get_ticks_msec() - CalculateHistoryStartTime) + "ms")
	# Set game variables
	GameVariables.InitialCalculation = false
	# Change scene
	get_tree().change_scene("res://Scenes/Game/Game.tscn")

# Calculates position, combat and arrival for active submarines at passed tick
func calculate_active_submarines_history_entry(tick : int) -> void:
	GameVariables.CurrentActiveSubmarines = GameVariables.ActiveSubmarines.duplicate(true)
	# Calculates current active submarines position
	for activesubmarine in GameVariables.CurrentActiveSubmarines:
		PositionFunctions.calculate_submarine_entry_position(tick, activesubmarine)
	# Calculates current active submarines combat and arrival
	for activesubmarine in GameVariables.CurrentActiveSubmarines:
		if GameVariables.ActiveSubmarines.has(activesubmarine):
			CombatFunctions.calculate_submarine_combat(tick, activesubmarine)
		else:
			CombatFunctions.calculate_submarine_target_outpost_effect(activesubmarine)

# Variable functions

# TODO: move
# Returns the name for a new submarine launched from passed outpost
func get_new_submarine_name(tick : int, outpost : String) -> String:
	# Get data
	var Data : Dictionary = GameVariables.OutpostData[outpost]
	# Get outpost launched submarine count at tick
	var LaunchedSubmarinesCount : int
	if Data["LaunchedSubmarines"].has(tick):
		LaunchedSubmarinesCount = len(Data["LaunchedSubmarines"][tick])
	# Get subnmarine name
	var SubmarineName : String = outpost + "-" + str(tick) + "-" + str(LaunchedSubmarinesCount)
	return SubmarineName

extends Node

# Calculates a submarine's positions until arrival
func calculate_submarine_all_positions(tick : int, submarine : String) -> void:
	# Get submarine data
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var History : Array = Data["History"]
	# Calculate submarine positions
	var CurrentTick : int = tick
	while GameVariables.ActiveSubmarines.has(submarine) and CurrentTick < GameVariables.VisibleTickMaximum - 1:
		CurrentTick += 1
		calculate_submarine_entry_position(CurrentTick, submarine)

# Calculates a submarine's position for a single history entry
func calculate_submarine_entry_position(tick : int, submarine : String) -> void:
	# Get submarine data
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var History : Array = Data["History"]
	# Get ticks
	var PreviousTick : int = tick - 1
	var CurrentTick : int = tick
	# Set history entry
	if CurrentTick > History[0]["Tick"]:
		# Get tick variables
		var PreviousHistoryIndex : int = HistoryFunctions.get_history_index(PreviousTick, History)
		var PreviousHistoryEntry : Dictionary = History[PreviousHistoryIndex]
		var CurrentHistoryIndex : int = PreviousHistoryIndex + 1
		# Get new current position
		var NewCurrentPosition : Vector2 = PreviousHistoryEntry["Position"]
		var Arrived : bool = false
		if tick >= History[0]["Tick"] + GameVariables.LaunchWaitTicks:
			# TODO: make work with submarines
			# Moves submarine speed from previous position towards previous target
			var TargetData : Dictionary = GameVariables.OutpostData[PreviousHistoryEntry["CurrentTarget"]]
			var TargetPosition : Vector2 = MapFunctions.get_closest_wrapped_position(PreviousHistoryEntry["Position"], TargetData["InitialPosition"])
			var DirectionAngle : float = PreviousHistoryEntry["Position"].angle_to_point(TargetPosition)
			NewCurrentPosition = PreviousHistoryEntry["Position"] + Vector2(GameVariables.DefaultSubmarineSpeed * GameVariables.SubmarineSpeedMultiplier * PreviousHistoryEntry["SpeedMultiplierTotal"], 0).rotated(DirectionAngle + PI)
			# Checks if submarien will arrive at target
			if Rect2(PreviousHistoryEntry["Position"], Vector2()).expand(NewCurrentPosition).intersects(Rect2(TargetPosition, Vector2()), true):
				# Sets arrived to true
				Arrived = true
				# Removes and history entries after arrival
				History.resize(CurrentHistoryIndex)
				# Removes submarine from active submarines
				GameVariables.ActiveSubmarines.erase(submarine)
		# Set new current position
		if Arrived == false:
			# Set new history entry
			if History[-1]["Tick"] < CurrentTick:
				var NewHistoryEntry : Dictionary = PreviousHistoryEntry.duplicate(true)
				NewHistoryEntry["Tick"] = CurrentTick
				History.append(NewHistoryEntry)
			# Sets current history entry position
			var CurrentHistoryEntry : Dictionary = History[CurrentHistoryIndex]
			CurrentHistoryEntry["Position"] = NewCurrentPosition
			# Set current history entry visible
			SonarFunctions.update_item_visible(CurrentTick, submarine)
	else:
		# Set current history entry visible
		SonarFunctions.update_item_visible(History[0]["Tick"], submarine)

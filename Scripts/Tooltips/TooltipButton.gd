extends Button

func _on_OptionWarningButton_mouse_entered() -> void:
	pressed = true

func _on_OptionWarningButton_mouse_exited() -> void:
	pressed = false

func _on_OptionWarningButton_toggled(buttonpressed : bool) -> void:
	for child in get_node("../..").get_children():
		if child != get_node(".."):
			if child == get_node("../../ToolTip"):
				child.visible = buttonpressed
			else:
				child.visible = not buttonpressed

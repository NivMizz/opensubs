extends "res://Scripts/Menu/MenuPanel.gd"

var GameInfoPlayerButton : PackedScene = preload("res://Scenes/InterfaceElements/GameInfoPlayerButton.tscn")

var GameId : int

func prepare() -> void:
	$ScrollContainer/GameInfoElements/JoinButton.visible = BackMenu != "JoinedGames"
	for child in $ScrollContainer/GameInfoElements/Players.get_children():
		if child.is_in_group("Permanent") == false:
			child.queue_free()
	Network.rpc_id(1, "get_game_info", GameId, get_tree().get_network_unique_id())

func set_game_info(gameinfo : Dictionary) -> void:
	for variable in gameinfo:
		var SetupVariableNode : Node = $ScrollContainer/GameInfoElements/Setup.get_node_or_null(variable)
		if SetupVariableNode != null:
			SetupVariableNode.set_value(gameinfo[variable])
	# Set occupied players
	for player in gameinfo["Players"]:
		if player != "Dormant":
			if gameinfo["Players"][player]["Occupied"]:
				var GameInfoPlayerButtonInstance : Node = GameInfoPlayerButton.instance()
				GameInfoPlayerButtonInstance.set_player(player, gameinfo["Players"][player])
				$ScrollContainer/GameInfoElements/Players.add_child(GameInfoPlayerButtonInstance)
	# Set unoccupied players
	for player in gameinfo["Players"]:
		if gameinfo["Players"][player]["Occupied"] == false:
			var GameInfoPlayerButtonInstance : Node = GameInfoPlayerButton.instance()
			$ScrollContainer/GameInfoElements/Players.add_child(GameInfoPlayerButtonInstance)

func _on_AdvancedOptionsButton_toggled(buttonpressed : bool) -> void:
	for child in $ScrollContainer/GameInfoElements/Setup.get_children():
		child.visible = child.is_in_group("Advanced") == false or buttonpressed

func _on_JoinButton_pressed():
	Menu.switch_menu("JoinedGames")
	Network.rpc_id(1, "join_game", GameId, get_tree().get_network_unique_id(), Settings.Username)


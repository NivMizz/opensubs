extends "res://Scripts/Menu/MenuPanel.gd"

func _on_GamesMenuButton_pressed():
	Menu.switch_menu("Games")

func _on_SettingsMenuButton_pressed():
	Menu.switch_menu("Settings")

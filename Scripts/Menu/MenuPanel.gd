extends Panel

onready var Menu : Node = get_node("/root/Menu")

export var BackMenu : String

func _on_BackButton_pressed():
	Menu.switch_menu(BackMenu)

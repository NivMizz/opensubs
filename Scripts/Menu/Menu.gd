extends Control

var Config := ConfigFile.new()

# Inbuilt functions

func _ready() -> void:
	var ConfigLoad := Config.load(get_config_path())
	if ConfigLoad != OK:
		show_popup("SelectServer")
	else:
		# Sets settings variables
		Settings.Username = Config.get_value("User", "Username")
		Settings.ServerIp = Config.get_value("Connection", "ServerIp")
		Settings.ServerPort = Config.get_value("Connection", "ServerPort")
		# Checks if connection settings are valid
		if get_connection_settings_valid():
			# Gets games
			switch_menu("Main")
			if get_tree().get_network_connected_peers().empty():
				Network.connect_to_server()

# Config functions

# Returns the location of the config file
func get_config_path() -> String:
	return OS.get_executable_path().get_base_dir().plus_file("Client.cfg")

# Returns true if connection settings are valid
func get_connection_settings_valid() -> bool:
	if Settings.Username == "":
		show_popup("InputUsername")
		return false
	elif Settings.ServerIp == "":
		show_popup("InputServerIp")
		return false
	elif Settings.ServerPort == 0:
		show_popup("InputServerPort")
		return false
	return true

# Menu functions

func switch_menu(menu : String) -> void:
	for child in $Panels.get_children():
		if child.name == menu:
			child.show()
			if child.has_method("prepare"):
				child.prepare()
		else:
			child.hide()

func show_game_info(gameid : int) -> void:
	for child in $Panels.get_children():
		if child.visible:
			$Panels/GameInfo.BackMenu = child.name
	$Panels/GameInfo.GameId = gameid
	switch_menu("GameInfo")

func show_popup(popup : String) -> void:
	var PopupNode : Node = $Popups.get_node(popup)
	PopupNode.show()
	if PopupNode.has_method("start_loading"):
		PopupNode.start_loading()
	$Popups/ColorRect.show()

func hide_popup(popup : String) -> void:
	var PopupNode : Node = $Popups.get_node(popup)
	PopupNode.hide()
	for child in $Popups.get_children():
		if child != $Popups/ColorRect:
			if child.visible:
				return
	$Popups/ColorRect.hide()

func connection_succeeded() -> void:
	$Panels/Settings.stop_connection_timer()
	hide_popup("ConnectingToServer")
	show_popup("ConnectionSucceeded")

func connection_failed() -> void:
	hide_popup("ConnectingToServer")
	show_popup("ConnectionFailed")

# Prompt buttons

func _on_DefaultServerButton_pressed() -> void:
	hide_popup("SelectServer")
	Settings.ServerIp = "5.161.129.232"
	Settings.ServerPort = 25565
	show_popup("InputUsername")

func _on_CustomServerButton_pressed() -> void:
	hide_popup("SelectServer")
	switch_menu("Settings")

func _on_InputUsernameOkButton_pressed() -> void:
	hide_popup("InputUsername")
	switch_menu("Settings")

func _on_InputServerIpOkButton_pressed() -> void:
	hide_popup("InputServerIp")
	switch_menu("Settings")

func _on_InputSeverPortOkButton_pressed() -> void:
	hide_popup("InputServerPort")
	switch_menu("Settings")

func _on_InputGameNameOkButton_pressed() -> void:
	hide_popup("InputGameName")

func _on_ConnectionSucceededOkButton_pressed() -> void:
	hide_popup("ConnectionSucceeded")

func _on_ConnectionFailedOkButton_pressed() -> void:
	hide_popup("ConnectionFailed")

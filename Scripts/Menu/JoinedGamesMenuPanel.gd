extends "res://Scripts/Menu/MenuPanel.gd"

var JoinedGameButton : PackedScene = preload("res://Scenes/GameButtons/JoinedGameButton.tscn")

func prepare() -> void:
	Network.rpc_id(1, "get_joined_games", get_tree().get_network_unique_id(), Settings.Username)

func set_games(gamesinfo : Dictionary) -> void:
	for child in $ScrollContainer/GameButtons.get_children():
		if child.is_in_group("Permanent") == false:
			child.queue_free()
	for gameid in gamesinfo.keys():
		var JoinedGameButtonInstance = JoinedGameButton.instance()
		JoinedGameButtonInstance.set_game(gameid, gamesinfo[gameid])
		$ScrollContainer/GameButtons.add_child(JoinedGameButtonInstance)
		$ScrollContainer/GameButtons.move_child(JoinedGameButtonInstance, 1)

func _on_RefreshGamesButton_pressed():
	Network.rpc_id(1, "get_joined_games", get_tree().get_network_unique_id(), Settings.Username)

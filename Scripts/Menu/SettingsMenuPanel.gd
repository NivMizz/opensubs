extends "res://Scripts/Menu/MenuPanel.gd"

export var ConnectToServerTime : int = 3

func prepare() -> void:
	# Set username tooltip visibility
	$"%UsernameLineEdit".text = str(Settings.Username)
	$"%UsernameErrorToolTipButton".visible = str(Settings.Username) == ""
	# Set server IP tooltip visibility
	$"%ServerIpLineEdit".text = str(Settings.ServerIp)
	$"%ServerIpErrorToolTipButton".visible = str(Settings.ServerIp) == ""
	# Set server port tooltip visibility
	if Settings.ServerPort != 0:
		$"%ServerPortLineEdit".text = str(Settings.ServerPort)
		$"%ServerPortErrorToolTipButton".hide()
	else:
		$"%ServerPortLineEdit".text = ""
		$"%ServerPortErrorToolTipButton".show()

# Returns true if connection settings are valid
func get_connection_settings_valid() -> bool:
	if Settings.Username == "":
		Menu.show_popup("InputUsername")
		return false
	elif Settings.ServerIp == "":
		Menu.show_popup("InputServerIp")
		return false
	elif Settings.ServerPort == 0:
		Menu.show_popup("InputServerPort")
		return false
	return true

func stop_connection_timer() -> void:
	$ConnectionTimer.disconnect("timeout", self, "_on_ConnectionTimer_timeout")
	$ConnectionTimer.stop()
	$ConnectionTimer.connect("timeout", self, "_on_ConnectionTimer_timeout")

func _on_ConnectToServerButton_pressed() -> void:
	var Username : String = $"%UsernameLineEdit".text
	var ServerIp : String = $"%ServerIpLineEdit".text
	var ServerPort : String = $"%ServerPortLineEdit".text
	# Sets settings variables
	Settings.Username = Username
	Settings.ServerIp = ServerIp
	Settings.ServerPort = int(ServerPort)
	# Checks if connection settings are valid
	if Menu.get_connection_settings_valid():
		# Writes setting variables to config file
		Menu.Config.set_value("User", "Username", Settings.Username)
		Menu.Config.set_value("Connection", "ServerIp", Settings.ServerIp)
		Menu.Config.set_value("Connection", "ServerPort", Settings.ServerPort)
		Menu.Config.save(Menu.get_config_path())
		# Starts connecting to server
		Network.connect_to_server()
		$ConnectionTimer.start(ConnectToServerTime)
		Menu.show_popup("ConnectingToServer")

func _on_UsernameLineEdit_text_changed(newtext : String) -> void:
	$"%UsernameErrorToolTipButton".visible = newtext == ""

func _on_ServerIpLineEdit_text_changed(newtext : String) -> void:
	$"%ServerIpErrorToolTipButton".visible = newtext == ""

func _on_ServerPortLineEdit_text_changed(newtext : String) -> void:
	$"%ServerPortErrorToolTipButton".visible = newtext == ""

func _on_ConnectionTimer_timeout():
	Network.close_server_connection()

func _on_BackButton_pressed():
	if get_tree().get_network_connected_peers().empty():
		Menu.show_popup("SelectServer")
	else:
		Menu.switch_menu(BackMenu)

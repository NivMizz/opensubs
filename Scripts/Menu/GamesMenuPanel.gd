extends "res://Scripts/Menu/MenuPanel.gd"

func _on_JoinedGamesMenuButton_pressed():
	Menu.switch_menu("JoinedGames")

func _on_PublicGamesMenuButton_pressed():
	Menu.switch_menu("PublicGames")

func _on_CreateGameMenuButton_pressed():
	Menu.switch_menu("CreateGame")

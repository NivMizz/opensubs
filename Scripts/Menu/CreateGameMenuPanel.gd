extends "res://Scripts/Menu/MenuPanel.gd"

func _ready():
	for ticklength in GameVariables.TickLengthsList:
		$"%TickLengthOptionButton".add_item(VariableFunctions.get_time_text(ticklength, true))
	$"%TickLengthOptionButton".select(6)
	for maptype in GameVariables.MapTypesList:
		$"%MapTypeOptionButton".add_item(maptype.capitalize())
	for outpostname in GameVariables.OutpostNamesList:
		$"%OutpostNamesOptionButton".add_item(outpostname.capitalize())

func _on_GameNameLineEdit_text_changed(newtext : String) -> void:
	$"%GameNameErrorToolTipButton".visible = newtext == ""

func _on_TickLengthOptionButton_item_selected(index : int) -> void:
	$"%TickLengthWarningToolTipButton".visible = GameVariables.TickLengthsList[index] < 10

func _on_AdvancedOptionsButton_toggled(buttonpressed : bool) -> void:
	for child in $ScrollContainer/SetupInterfaceElements.get_children():
		child.visible = child.is_in_group("Advanced") == false or buttonpressed

func _on_CreateGameButton_pressed() -> void:
	var GameName : String = $"%GameNameLineEdit".text
	if GameName == "":
		Menu.show_popup("InputGameName")
		return
	var GameInfo : Dictionary = {
		# Basic setup variables
		"GameName" : GameName,
		"PlayerCount" : $"%PlayerCountScrollWheel".StepValue,
		"TickLength" : GameVariables.TickLengthsList[$"%TickLengthOptionButton".selected],
		"MapType" : GameVariables.MapTypesList[$"%MapTypeOptionButton".selected],
		"OutpostsPerPlayer" : $"%OutpostsPerPlayerScrollWheel".StepValue,
		"GenerateTroopsTicksDivisor" : $"%GenerateTroopsTicksDivisorScrollWheel".StepValue,
		"GenerateMaximumShieldTicksDivisor" : $"%GenerateMaximumShieldTicksDivisorScrollWheel".StepValue,
		"SonarRangeMultiplier" : $"%SonarRangeMultiplierScrollWheel".StepValue,
		"SubmarineSpeedMultiplier" : $"%SubmarineSpeedMultiplierScrollWheel".StepValue,
		# Advanced setup variables
		"OutpostNames" : GameVariables.OutpostNamesList[$"%OutpostNamesOptionButton".selected],
		"LaunchWaitTicks" : $"%LaunchWaitTicksScrollWheel".StepValue,
		"GiftWaitTicks" : $"%GiftWaitTicksScrollWheel".StepValue,
		# Unimplemented setup variables
		"OutpostStartingTroops" : 40,
		"OutpostSpacing" : 300,
		"GenerateTroopsNumber" : 6
	}
	Network.rpc_id(1, "create_game", get_tree().get_network_unique_id(), Settings.Username, GameInfo)
	Menu.switch_menu("JoinedGames")

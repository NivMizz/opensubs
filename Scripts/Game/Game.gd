extends Node2D

export var DistanceToHoverOutpost : float = 50.0
export var MapCursorMovementAllowance : float = 10.0

var Outpost : PackedScene = preload("res://Scenes/Outposts/Outpost.tscn")
var Submarine : PackedScene = preload("res://Scenes/Submarines/Submarine.tscn")
var SubmarineLines : PackedScene = preload("res://Scenes/Lines/SubmarineLines.tscn")
var SonarRangeMasks : PackedScene = preload("res://Scenes/Outposts/SonarRangeMasks.tscn")
var OnlineIndicator : PackedScene = preload("res://Scenes/InterfaceElements/OnlineIndicator.tscn")

var Jumping : bool = false
var CursorPosition : Vector2
var PressedMapCursorPosition : Vector2
var PreviousCameraPosition : Vector2
var HoveredStartOutpost : Node
var HoveredEndOutpost : Node
var MapCursorMovementMaximum : float
var CurrentZoom : float = 1.0
var ZoomMultiplier : float = 2.0
var OutpostPanels : Array = ["Factory", "Generator", "ObscuredOutpost", "Preparing"]
var SubmarinePanels : Array = ["Preparing", "Launching", "Submarine", "ObscuredSubmarine"]

# Inbuilt functions

func _ready() -> void:
	randomize()
	GameVariables.GameNode = self
	set_tick(GameVariables.GlobalTick)
	update_players()
	update_online_players()
	# Set outposts
	var OutpostLoadStartTime : int = Time.get_ticks_msec()
	for outpost in GameVariables.OutpostData:
		var OutpostInstance := Outpost.instance()
		OutpostInstance.name = outpost
		set_node_variables(OutpostInstance, GameVariables.OutpostData[outpost])
		$Outposts.add_child(OutpostInstance)
	print("Loading outposts instances took " + str(Time.get_ticks_msec() - OutpostLoadStartTime) + "ms")
	# Set submarines
	var SubmarineLoadStartTime : int = Time.get_ticks_msec()
	for submarine in GameVariables.SubmarineData:
		set_submarine_data(submarine)
	print("Loading submarines instances took " + str(Time.get_ticks_msec() - SubmarineLoadStartTime) + "ms")
	# Set camera starting position
	var AnchorPlayerOutpostPosition : Vector2
	var PlayerOutpostsWrappedPositions : Array
	for outpost in GameVariables.OutpostData:
		var OutpostData : Dictionary = GameVariables.OutpostData[outpost]
		var OutpostHistory : Array = OutpostData["History"]
		var OutpostHistoryEntry : Dictionary = OutpostHistory[0]
		if OutpostHistoryEntry["CurrentPlayer"] == GameVariables.Player:
			var OutpostWrappedPosition : Vector2
			if PlayerOutpostsWrappedPositions.empty():
				AnchorPlayerOutpostPosition = OutpostData["InitialPosition"]
				OutpostWrappedPosition = OutpostData["InitialPosition"]
			else:
				OutpostWrappedPosition = MapFunctions.get_closest_wrapped_position(AnchorPlayerOutpostPosition, OutpostData["InitialPosition"])
			PlayerOutpostsWrappedPositions.append(OutpostWrappedPosition)
	var CameraPosition : Vector2
	for playeroutpostposition in PlayerOutpostsWrappedPositions:
		CameraPosition += playeroutpostposition
	CameraPosition = CameraPosition / len(PlayerOutpostsWrappedPositions)
	$Camera2D.global_position = MapFunctions.get_wrapped_position(CameraPosition)
	update_map()
	# Unpause game
	if GameVariables.StartTime != -1:
		unpause_game()
	# Set order buttons
	for ordertick in GameVariables.OrdersKeys:
		for orderid in GameVariables.Orders[ordertick]:
			var Order : Dictionary = GameVariables.Orders[ordertick][orderid]
			$Interface/Panels/Time.set_queued_order(Order)

func _process(delta : float) -> void:
	if $Interface/Panels/Communications.visible == false:
		if Input.is_action_just_released("ZoomIn") or Input.is_action_just_released("ZoomOut"):
			if Input.is_action_just_released("ZoomIn"):
				CurrentZoom -= ZoomMultiplier * delta
			elif Input.is_action_just_released("ZoomOut"):
				CurrentZoom += ZoomMultiplier * delta
			CurrentZoom = clamp(CurrentZoom, 0.1, min(GameVariables.MapSize.x, GameVariables.MapSize.y) / max(OS.get_window_size().x, OS.get_window_size().y))
			$Camera2D.zoom = Vector2(CurrentZoom, CurrentZoom)
	if $MapControls/MapButton.pressed and $MapTween.is_active() == false:
		$Camera2D.global_position += PressedMapCursorPosition - get_global_mouse_position()
		if $Camera2D.global_position != PreviousCameraPosition:
			update_map()
		PreviousCameraPosition = $Camera2D.global_position
		var MapCursorMovementDifference : float = PressedMapCursorPosition.distance_to(get_global_mouse_position())
		if MapCursorMovementDifference > MapCursorMovementMaximum:
			MapCursorMovementMaximum = MapCursorMovementDifference
	HoveredEndOutpost = null
	for child in $Outposts.get_children():
		if child != HoveredStartOutpost:
			var OutpostPosition : Vector2 = child.rect_global_position + child.rect_pivot_offset
			if get_global_mouse_position().distance_to(OutpostPosition) <= DistanceToHoverOutpost:
				HoveredEndOutpost = child
				child.focus()
				CursorPosition = OutpostPosition
			elif child.Focused:
				child.unfocus()
	if HoveredEndOutpost == null:
		CursorPosition = get_global_mouse_position()

# Tick functions

func update_tick() -> void:
	for child in $Submarines.get_children():
		child.update_tick()
	for child in $Outposts.get_children():
		child.update_tick()
	update_tick_panels()

func set_tick(tick : int) -> void:
	GameVariables.VisibleTick = min(tick, GameVariables.VisibleTickMaximum - 1)
	update_tick()

func set_global_tick(globaltick : int) -> void:
	print("Set global tick to " + str(globaltick))
	var PreviousGlobalTick : int = GameVariables.GlobalTick
	GameVariables.GlobalTick = globaltick
	GameVariables.VisibleTickMaximum = GameVariables.GlobalTick + Settings.VisibleTickMaximumAhead
	# Sets history expansion
	HistoryFunctions.calculate_history_expansion()
	# Updates items visibility
	for outpost in GameVariables.OutpostData:
		SonarFunctions.update_obscured_item_previous_sonar_total(PreviousGlobalTick, outpost)
	for submarine in GameVariables.SubmarineData:
		SonarFunctions.update_obscured_item_previous_sonar_total(PreviousGlobalTick, submarine)
	# Calculates obscured submarine effect if arrived before global tick
	for submarine in GameVariables.SubmarineData:
		var SubmarineData : Dictionary = GameVariables.SubmarineData[submarine]
		var SubmarineHistory : Array = SubmarineData["History"]
		var ArrivalTick : int = SubmarineHistory[-1]["Tick"] + 1
		if ArrivalTick > PreviousGlobalTick and ArrivalTick <= GameVariables.GlobalTick:
			# Get target data
			var Target : String = SubmarineHistory[-1]["CurrentTarget"]
			var TargetData : Dictionary = DataFunctions.get_item_data(Target)
			var TargetHistory : Array = TargetData["History"]
			var TargetHistoryEntry : Dictionary = HistoryFunctions.get_history_entry(ArrivalTick, TargetHistory)
			# Calculates submarine effect
			if TargetHistoryEntry["VisibleTotal"] > 0:
				CombatFunctions.calculate_submarine_target_outpost_effect(submarine)
	# Checks if the ruler starting tick should be increased alongside global tick
	if $Rulers.RulerStartTick == PreviousGlobalTick:
		$Rulers.RulerStartTick = GameVariables.GlobalTick
	# Removes past queued order buttons
	$Interface/Panels/Time.remove_past_orders()
	# Checks if the global tick should be set
	if GameVariables.VisibleTick + 1 == GameVariables.GlobalTick and Jumping == false:
		GameVariables.VisibleTick = GameVariables.GlobalTick
	# Updates tick
	update_tick()
	# Gets all orders revealed on this tick
	Network.rpc_id(
		1,
		"get_revealed_orders",
		# Game ID
		GameVariables.GameId,
		# Client ID
		get_tree().get_network_unique_id(),
		# Username
		Settings.Username,
		# Global tick
		GameVariables.GlobalTick
	)

func set_submarine_toasts(submarine : String) -> void:
	$Interface/Panels/Launching.set_submarine_toasts(submarine)
	$Interface/Panels/Submarine.set_submarine_toasts(submarine)

# Unpauses the game
func unpause_game() -> void:
	var TimeSinceStart : int = OS.get_unix_time() - GameVariables.StartTime
	$TickTimer.start(GameVariables.TickLength - (TimeSinceStart % GameVariables.TickLength))

# Jumps time control to passed tick
func jump_to_tick(tick : int) -> void:
	Jumping = true
	$TickTween.interpolate_property(GameVariables, "VisibleTick", GameVariables.VisibleTick, min(tick, GameVariables.VisibleTickMaximum - 1), 1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$TickTween.start()
	show_panel("Time")

# Jumps time control to submarine arrival and jumps camera position to target position
func jump_to_arrival(submarine : String) -> void:
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var History : Array = Data["History"]
	# TODO: make work with submarines
	# Jumps camera position to target position
	var EndTarget : String = History[-1]["CurrentTarget"]
	var EndTargerData : Dictionary = GameVariables.OutpostData[EndTarget]
	var EndTargetPosition : Vector2 = EndTargerData["InitialPosition"]
	var EndTargetWrappedPosition : Vector2 = MapFunctions.get_wrapped_position(EndTargetPosition)
	$MapTween.interpolate_property($Camera2D, "global_position", $Camera2D.global_position, EndTargetWrappedPosition, 1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$MapTween.start()
	# Jumps time control to submarine arrival
	jump_to_tick(History[-1]["Tick"] + 1)

# Data functions

func set_submarine_data(submarine : String) -> void:
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var ExistingSubmarine : Node = $Submarines.get_node_or_null(submarine)
	if ExistingSubmarine != null:
		set_node_variables(ExistingSubmarine, Data)
	else:
		var SubmarineInstance : Node = Submarine.instance()
		SubmarineInstance.name = submarine
		set_node_variables(SubmarineInstance, Data)
		$Submarines.add_child(SubmarineInstance)
		SubmarineInstance.prepare()

func set_node_variables(node : Node, variables : Dictionary):
	for variable in variables:
		if node.get(variable) != null:
			node.set(variable, variables[variable])

func create_submarine() -> void:
	var InitialTick : int = $Rulers.RulerStartTick
	if HoveredStartOutpost != null and HoveredEndOutpost != null:
		# TODO: make work with submarines
		var HoveredStartOutpostHistoryEntry : Dictionary = HistoryFunctions.get_history_entry(InitialTick, HoveredStartOutpost.History)
		if HoveredStartOutpostHistoryEntry["CurrentPlayer"] == GameVariables.Player:
			for tick in HoveredStartOutpost.Submarines:
				if tick >= InitialTick and tick < InitialTick + GameVariables.LaunchWaitTicks:
					for outpostsubmarine in HoveredStartOutpost.Submarines[tick]:
						if outpostsubmarine != "Cancelled":
							var OutpostSubmarineHistory : Array = GameVariables.SubmarineData[outpostsubmarine]["History"]
							var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(InitialTick, OutpostSubmarineHistory)
							if HistoryEntry.empty() == false and HistoryEntry["CurrentTarget"] == HoveredEndOutpost.name:
								focus_submarine(outpostsubmarine)
								return
			var OrderId : int = randi()
			var LaunchTime : int = -1
			if GameVariables.StartTime != -1:
				LaunchTime = OS.get_unix_time()
			var LaunchOrder : Dictionary = {
				"Player" : GameVariables.Player,
				"Time" : LaunchTime,
				"Type" : "Launch",
				"InitialOutpost" : HoveredStartOutpost.name,
				"InitialTarget" : HoveredEndOutpost.name,
				"InitialTroops" : 0
			}
			Network.rpc_id(
				1,
				"set_order",
				# Game ID
				GameVariables.GameId,
				# Client ID
				get_tree().get_network_unique_id(),
				# Tick
				InitialTick,
				# Order ID
				OrderId,
				# Order
				LaunchOrder
			)
			OrderFunctions.set_order(InitialTick, OrderId, LaunchOrder)
			var Submarine : String = OrderFunctions.get_submarine_from_launch_order(LaunchOrder)
			focus_submarine(Submarine)

func cancel_submarine(submarine : String) -> void:
	var SubmarineNode : Node = $Submarines.get_node(submarine)
	SubmarineNode.cancel_submarine()
	for panel in SubmarinePanels:
		if $Interface/Panels.get_node(panel).Submarine == submarine:
			hide_panel(panel)

func set_submarine_temporary_troops(submarine : String, value : int) -> void:
	var SubmarineNode : Node = $Submarines.get_node(submarine)
	SubmarineNode.set_temporary_troops(value)

func override_submarine_temporary_troops(submarine : String) -> void:
	var SubmarineNode : Node = $Submarines.get_node(submarine)
	SubmarineNode.TemporaryTroops = false
	SubmarineNode.update_tick()

func set_outpost_temporary_troops(outpost : String, value : int) -> void:
	var OutpostNode : Node = $Outposts.get_node(outpost)
	OutpostNode.set_temporary_troops(value)

func override_outpost_temporary_troops(outpost : String) -> void:
	var OutpostNode : Node = $Outposts.get_node(outpost)
	OutpostNode.TemporaryTroops = false
	OutpostNode.update_tick()
	$Interface/Panels/Preparing.TemporaryTroops = false
	$Interface/Panels/Preparing.update_tick()
	$Interface/Panels/Launching.TemporaryTroops = false
	$Interface/Panels/Launching.update_tick()

func set_hovered_start_outpost(outpostnode : Node) -> void:
	HoveredStartOutpost = outpostnode
	if HoveredEndOutpost == outpostnode:
		HoveredEndOutpost = null
	$Rulers.RulerStartTick = GameVariables.VisibleTick

func set_factory_panel_info(factorypanelhistory : Array) -> void:
	$Interface/Panels/Factory.History = factorypanelhistory
	$Interface/Panels/Factory.update_tick()

func set_submarine_panel_info(submarinepanelhistory : Array) -> void:
	$Interface/Panels/Submarine.History = submarinepanelhistory
	$Interface/Panels/Submarine.update_tick()

# Gets information to display on submarine panels
func get_submarine_info(submarine : String) -> String:
	var Data : Dictionary = GameVariables.SubmarineData[submarine]
	var History : Array = Data["History"]
	var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(GameVariables.VisibleTick, History)
	var Info : String = "Drillers"
	if HistoryEntry.empty() == false:
		var TimeToArrival : int = ((History[-1]["Tick"] + 1) - GameVariables.VisibleTick) * GameVariables.TickLength
		Info += "\nArrives in " + VariableFunctions.get_time_text(TimeToArrival)
		Info += "\nSpeed multiplier " + str(HistoryEntry["SpeedMultiplierTotal"])
	return Info

# Node functions

func get_source_node(source : String) -> Node:
	var OutpostNode : Node = $Outposts.get_node_or_null(source)
	if OutpostNode != null:
		return OutpostNode
	var SubmarineNode : Node = $Submarines.get_node_or_null(source)
	if SubmarineNode != null:
		return SubmarineNode
	# Failure case
	print("\"" + source + "\" is not a valid node")
	return Node.new()

# Visual functions

func update_map() -> void:
	# Set background size
	$Background.rect_position = Vector2(stepify($Camera2D.global_position.x, 50), stepify($Camera2D.global_position.y, 50)) - (GameVariables.MapSize / 2) - Vector2(100, 100)
	$Background.rect_size = GameVariables.MapSize + Vector2(200, 200)
	# Update positions
	for child in $Outposts.get_children():
		child.update_position()
	for child in $Submarines.get_children():
		child.update_position()

func update_players() -> void:
	var OccupiedPlayers : Array
	for player in GameVariables.Players:
		if GameVariables.Players[player]["Username"] != "Dormant":
			if GameVariables.Players[player]["Occupied"]:
				OccupiedPlayers.append(player)
	if len(OccupiedPlayers) < len(GameVariables.Players) - 1:
		$Interface/GameStartLabel.text = "Waiting for more players to join (" + str(len(OccupiedPlayers)) + "/" + str(len(GameVariables.Players) - 1) + " present)"
	else:
		$Interface/GameStartLabel.hide()
	$Interface/Panels/Communications.prepare()

func focus_outpost(outpost : String, focuscamera : bool = false) -> void:
	unfocus_outposts(outpost)
	unfocus_submarines()
	var OutpostNode : Node = $Outposts.get_node(outpost)
	# Set camera position to outpost position
	if focuscamera:
		$Camera2D.global_position = OutpostNode.global_position
		update_map()
	# Set panels outpost to outpost
	for panel in OutpostPanels:
		$Interface/Panels.get_node(panel).Outpost = outpost
	var CurrentType : String = OutpostNode.get_current_type(GameVariables.VisibleTick)
	switch_outpost_panels([CurrentType])
	# Focus outpost
	OutpostNode.Selected = true
	OutpostNode.focus()

func focus_submarine(submarine : String, focuscamera : bool = false) -> void:
	unfocus_outposts()
	unfocus_submarines(submarine)
	var SubmarineNode : Node = $Submarines.get_node(submarine)
	# Set camera position to submarine position
	if focuscamera:
		$Camera2D.global_position = SubmarineNode.global_position
		update_map()
	# Set panels submarine to submarine
	for panel in OutpostPanels:
		var PanelNode : Node = $Interface/Panels.get_node(panel)
		PanelNode.Outpost = SubmarineNode.InitialOutpost
		if SubmarinePanels.has(panel) == false:
			PanelNode.prepare()
	# Set panels outpost to submarine initial outpost
	for panel in SubmarinePanels:
		var PanelNode : Node = $Interface/Panels.get_node(panel)
		PanelNode.Submarine = submarine
		PanelNode.prepare()
	# Focus submarine
	SubmarineNode.focus()

func unfocus_outposts(exception : String = "") -> void:
	switch_outpost_panels([])
	for child in $Outposts.get_children():
		child.Selected = false
	$Rulers.hide()
	if HoveredEndOutpost != null:
		if HoveredEndOutpost.name != exception:
			HoveredEndOutpost.unfocus()
			HoveredEndOutpost = null

func unfocus_submarines(exception : String = "") -> void:
	switch_submarine_panels([])
	for child in $Submarines.get_children():
		if child.name != exception:
			child.unfocus()
			child.Selected = false

func show_panel(panel : String) -> void:
	var PanelNode : Node = $Interface/Panels.get_node(panel)
	if PanelNode.visible == false:
		match panel:
			"Communications":
				$Interface/BottomBar/HBoxContainer/CommunicationsButton.pressed = true
			"Time":
				$Interface/BottomBar/HBoxContainer/ClockButton.pressed = true
		PanelNode.show()
		if PanelNode.has_method("prepare"):
			PanelNode.prepare()

func hide_panel(panel : String) -> void:
	var PanelNode : Node = $Interface/Panels.get_node(panel)
	if PanelNode.visible:
		match panel:
			"Communications":
				$Interface/BottomBar/HBoxContainer/CommunicationsButton.pressed = false
			"Time":
				$Interface/BottomBar/HBoxContainer/ClockButton.pressed = false
		PanelNode.hide()

func hide_all_panels() -> void:
	for panelnode in $Interface/Panels.get_children():
		hide_panel(panelnode.name)

func update_tick_panels() -> void:
	for child in $Interface/Panels.get_children():
		if child.has_method("update_tick"):
			child.update_tick()

func switch_submarine_panels(panels : Array) -> void:
	for panel in SubmarinePanels:
		if panels.has(panel):
			show_panel(panel)
		else:
			hide_panel(panel)

func switch_outpost_panels(panels : Array) -> void:
	for panel in OutpostPanels:
		if panels.has(panel):
			show_panel(panel)
		else:
			hide_panel(panel)

func show_popup(popup : String) -> void:
	var PopupNode : Node = $Interface/Popups.get_node(popup)
	PopupNode.show()
	match popup:
		"Exit":
			$Interface/BottomBar/HBoxContainer/ExitButton.pressed = true
	$Interface/Popups/ColorRect.show()

func hide_popup(popup : String) -> void:
	var PopupNode : Node = $Interface/Popups.get_node(popup)
	PopupNode.hide()
	match popup:
		"Exit":
			$Interface/BottomBar/HBoxContainer/ExitButton.pressed = false
	for child in $Interface/Popups.get_children():
		if child != $Interface/Popups/ColorRect:
			if child.visible:
				return
	$Interface/Popups/ColorRect.hide()

func update_online_players() -> void:
	for child in $Interface/OnlinePlayers/OnlineIndicators.get_children():
		child.queue_free()
	var DisplayOnlinePlayers : Array = GameVariables.OnlinePlayers.duplicate(true)
	DisplayOnlinePlayers.erase(GameVariables.Player)
	for i in len(DisplayOnlinePlayers):
		var OnlineIndicatorInstance = OnlineIndicator.instance()
		OnlineIndicatorInstance.rect_position.x = -25 * i
		OnlineIndicatorInstance.modulate = Color(GameVariables.Colors[DisplayOnlinePlayers[i]])
		$Interface/OnlinePlayers/OnlineIndicators.add_child(OnlineIndicatorInstance)
		$Interface/OnlinePlayers/OnlinePlayersLabel.rect_position.x = -90 - 25 * (i + 1)
	$Interface/OnlinePlayers/OnlinePlayersLabel.visible = not DisplayOnlinePlayers.empty()

func update_messages(chat : Array) -> void:
	if $Interface/Panels/Communications.visible:
		$Interface/Panels/Communications.update_messages(chat)

# Bottom bar buttons

func _on_ExitButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		show_popup("Exit")
	else:
		hide_popup("Exit")

func _on_CommunicationsButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		hide_panel("Time")
		show_panel("Communications")
	else:
		hide_panel("Communications")

func _on_ClockButton_toggled(buttonpressed : bool) -> void:
	if buttonpressed:
		hide_panel("Communications")
		show_panel("Time")
	else:
		hide_panel("Time")
		set_tick(GameVariables.GlobalTick)

# Popup buttons

func _on_ExitYesButton_pressed() -> void:
	Network.rpc_id(1, "remove_player_id", GameVariables.GameId, get_tree().get_network_unique_id())
	GameVariables.set_script(null)
	GameVariables.set_script(preload("res://Scripts/SingletonVariables/GameVariables.gd"))
	get_tree().change_scene("res://Scenes/Menu/Menu.tscn")

func _on_ExitNoButton_pressed() -> void:
	hide_popup("Exit")

func _on_AbsentChatPlayersOkButton_pressed() -> void:
	hide_popup("AbsentChatPlayers")

# Map buttons

func _on_MapButton_button_down() -> void:
	MapCursorMovementMaximum = 0
	PressedMapCursorPosition = get_global_mouse_position()
	PreviousCameraPosition = $Camera2D.global_position

func _on_MapButton_button_up() -> void:
	if MapCursorMovementMaximum <= MapCursorMovementAllowance:
		hide_all_panels()
		unfocus_outposts()
		unfocus_submarines()

# Tweens

func _on_TickTween_tween_step(object : Object, key : NodePath, elapsed : float, value : float) -> void:
	set_tick(int(value))

func _on_TickTween_tween_completed(object : Object, key : NodePath) -> void:
	Jumping = false

func _on_MapTween_tween_step(object : Object, key : NodePath, elapsed : float, value : Vector2) -> void:
	update_map()

func _on_MapTween_tween_completed(object : Object, key : NodePath) -> void:
	if $MapControls/MapButton.pressed:
		PressedMapCursorPosition = get_global_mouse_position()

# Timers

func _on_TickTimer_timeout() -> void:
	set_global_tick(GameVariables.GlobalTick + 1)

# Visibility

func _on_Game_draw():
	update_map()

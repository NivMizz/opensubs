extends "res://Scripts/GameButtons/GameButton.gd"

func _on_InteractButton_pressed():
	get_node("/root/Menu").switch_menu("JoinedGames")
	Network.rpc_id(1, "join_game", GameId, get_tree().get_network_unique_id(), Settings.Username)

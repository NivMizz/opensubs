extends Control

var GameId : int

func set_game(gameid : int, game : Dictionary) -> void:
	GameId = gameid
	# Get occupied players
	var OccupiedPlayers : int = -1
	for player in game["Players"]:
		if game["Players"][player]["Occupied"]:
			OccupiedPlayers += 1
	# Set name button text
	$NameButton.text = game["GameName"] + " - " + str(OccupiedPlayers) + "/" + str(len(game["Players"]) - 1) + " players"

func _on_NameButton_pressed():
	get_node("/root/Menu").show_game_info(GameId)

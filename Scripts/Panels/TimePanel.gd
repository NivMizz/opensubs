extends Control

export var AdjustMultiplier : float = 0.08

var QueuedOrderButton : PackedScene = preload("res://Scenes/InterfaceElements/QueuedOrder.tscn")

var PressedAngle : float
var Value : float
var PreviousValue : float
var PreviousCursorAngle : float

func _process(_delta : float) -> void:
	update_time_label()
	$Wheel/WheelSprite.rotation_degrees = Value / AdjustMultiplier
	if $Wheel/WheelButton.pressed and GameVariables.GameNode.Jumping == false:
		if get_cursor_angle() != PreviousCursorAngle:
			$Wheel/WheelPoint.look_at(get_global_mouse_position())
			Value = PreviousValue + ($Wheel/WheelPoint.rotation_degrees - PressedAngle) * AdjustMultiplier
			clamp_value()
			if int(Value) != GameVariables.VisibleTick:
				GameVariables.GameNode.set_tick(int(Value))

func prepare() -> void:
	Value = GameVariables.VisibleTick
	$ExpandButton.prepare()

func update_tick() -> void:
	if GameVariables.GameNode.Jumping:
		Value = GameVariables.VisibleTick

func set_queued_order(order : Dictionary) -> void:
	if order["Player"] == GameVariables.Player:
		if order["Tick"] > GameVariables.GlobalTick:
			var QueuedOrderButtonInstance := QueuedOrderButton.instance()
			QueuedOrderButtonInstance.Order = order
			QueuedOrderButtonInstance.prepare()
			var PositionInParent : int
			for i in len($ScrollContainer/QueuedOrders.get_children()):
				if (
					$ScrollContainer/QueuedOrders.get_child(i).Order["Tick"] <= order["Tick"] and
					(
						i + 1 >= len($ScrollContainer/QueuedOrders.get_children()) or
						$ScrollContainer/QueuedOrders.get_child(i + 1).Order["Tick"] > order["Tick"]
					)
				):
					PositionInParent = i + 1
					break
			$ScrollContainer/QueuedOrders.add_child(QueuedOrderButtonInstance)
			$ScrollContainer/QueuedOrders.move_child(QueuedOrderButtonInstance, PositionInParent)

func remove_queued_order(order : Dictionary) -> void:
	for child in $ScrollContainer/QueuedOrders.get_children():
		if (
			child.Order["Tick"] == order["Tick"] and
			child.Order["OrderId"] == order["OrderId"]
		):
			child.queue_free()

func update_queued_order_status(order : Dictionary) -> void:
	for child in $ScrollContainer/QueuedOrders.get_children():
		if (
			child.Order["Tick"] == order["Tick"] and
			child.Order["OrderId"] == order["OrderId"]
		):
			child.update_order_status()

func remove_past_orders() -> void:
	for child in $ScrollContainer/QueuedOrders.get_children():
		if child.Order["Tick"] <= GameVariables.GlobalTick:
			child.queue_free()

func clamp_value() -> void:
	Value = clamp(Value, 0, GameVariables.VisibleTickMaximum - 1)

func update_time_label() -> void:
	var Time : int = (GameVariables.VisibleTick - GameVariables.GlobalTick) * GameVariables.TickLength
	if Time >= 0:
		$TimeLabel.text = VariableFunctions.get_time_text(Time)  + " from now"
	else:
		$TimeLabel.text = VariableFunctions.get_time_text(-Time)  + " ago"

func get_cursor_angle() -> float:
	return rad2deg($Wheel/WheelPoint.global_position.angle_to(get_global_mouse_position()))

func _on_WheelButton_button_down() -> void:
	$Wheel/WheelPoint.look_at(get_global_mouse_position())
	PressedAngle = $Wheel/WheelPoint.rotation_degrees
	PreviousValue = Value
	PreviousCursorAngle = get_cursor_angle()

func _on_WheelButton_button_up() -> void:
	var WheelCursorMovement : float = get_global_mouse_position().x - $Wheel/WheelPoint.global_position.x
	if get_cursor_angle() == PreviousCursorAngle:
		Value += WheelCursorMovement / abs(WheelCursorMovement)
		clamp_value()
		if int(Value) != GameVariables.VisibleTick:
			GameVariables.GameNode.set_tick(int(Value))

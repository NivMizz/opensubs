extends "res://Scripts/Panels/OutpostPanel.gd"

func prepare() -> void:
	update_tick()

func get_outpost_info() -> String:
	# Get outpost data
	var OutpostData : Dictionary = GameVariables.OutpostData[Outpost]
	var OutpostHistory : Array = OutpostData["History"]
	var OutpostHistoryEntry : Dictionary = OutpostHistory[GameVariables.VisibleTick]
	var TotalGenerateTroopsTicks : int = GameVariables.DefaultGenerateTroopsTicks / GameVariables.GenerateTroopsTicksDivisor
	return (
		"\n+" + str(OutpostHistoryEntry["TroopsNumberMultipliersTotal"] * GameVariables.GenerateTroopsNumber) +
		" in " + VariableFunctions.get_time_text(get_ticks_to_production() * GameVariables.TickLength) +
		" and every " + VariableFunctions.get_time_text(int(ceil(TotalGenerateTroopsTicks * OutpostHistoryEntry["TroopsTicksMultipliersTotal"])) * GameVariables.TickLength)
		)

func hide_unused_buttons() -> void:
	# Get outpost data
	var OutpostData : Dictionary = GameVariables.OutpostData[Outpost]
	var OutpostHistory : Array = OutpostData["History"]
	var OutpostHistoryEntry : Dictionary = OutpostHistory[GameVariables.VisibleTick]
	# If outpost is not dormant, show button to jump to production
	$ClockButton.visible = GameVariables.Players[OutpostHistoryEntry["CurrentPlayer"]]["Username"] != "Dormant"

func get_ticks_to_production() -> int:
	# Get outpost data
	var OutpostData : Dictionary = GameVariables.OutpostData[Outpost]
	var OutpostHistory : Array = OutpostData["History"]
	var OutpostHistoryEntry : Dictionary = OutpostHistory[GameVariables.VisibleTick]
	# Get ticks to next troop production
	var TicksToProduction : int = OutpostHistoryEntry["TroopsTicksCountdown"]
	if TicksToProduction == 0:
		TicksToProduction = GameVariables.DefaultGenerateTroopsTicks / GameVariables.GenerateTroopsTicksDivisor
	return TicksToProduction

func _on_ClockButton_pressed() -> void:
	GameVariables.GameNode.jump_to_tick(GameVariables.VisibleTick + get_ticks_to_production())

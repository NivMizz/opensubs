extends Panel

var Outpost : String

func _ready() -> void:
	hide()

func prepare() -> void:
	update_tick()

func update_tick() -> void:
	if visible:
		# Get outpost data
		var OutpostData : Dictionary = GameVariables.OutpostData[Outpost]
		var OutpostHistory : Array = OutpostData["History"]
		var OutpostHistoryEntry : Dictionary = OutpostHistory[GameVariables.VisibleTick]
		# Set header elements
		var Player : String = OutpostHistoryEntry["CurrentPlayer"]
		$Header.modulate = Color(GameVariables.Colors[Player])
		$PlayerLabel.text = GameVariables.Players[Player]["Username"]

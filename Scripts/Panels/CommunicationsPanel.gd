extends Control

var ChatButton : PackedScene = preload("res://Scenes/InterfaceElements/ChatButton.tscn")
var PlayerPillsContainer : PackedScene = preload("res://Scenes/InterfaceElements/PlayerPillsContainer.tscn")
var PlayerPill : PackedScene = preload("res://Scenes/InterfaceElements/PlayerPill.tscn")
var Message : PackedScene = preload("res://Scenes/InterfaceElements/Message.tscn")

var CurrentPlayers : Array
var MaximumScroll : int

func _ready() -> void:
	MaximumScroll = $Messages/ScrollContainer.get_v_scrollbar().max_value
	$Messages/ScrollContainer.get_v_scrollbar().connect("changed", self, "jump_to_messages_end")

func _process(delta : float) -> void:
	if visible:
		if $Messages/TextEdit.has_focus():
			if Input.is_action_just_pressed("SendMessage"):
				$Messages/TextEdit.text[-1] = ""
				send_message()

func prepare() -> void:
	$ExpandButton.prepare()
	$Chats.show()
	$Messages.hide()
	update_chats()

func update_chats() -> void:
	for child in $Chats/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("ChatButton"):
			child.queue_free()
	for chat in GameVariables.Chats:
		var ChatButtonInstance := ChatButton.instance()
		ChatButtonInstance.Players = chat
		ChatButtonInstance.set_message(GameVariables.Chats[chat][-1])
		$Chats/ScrollContainer/VBoxContainer.add_child(ChatButtonInstance)

func set_players(players : Array, playerselection : bool = false) -> void:
	var Player : String = GameVariables.Player
	if playerselection:
		CurrentPlayers.append(Player)
		var ChatPlayers : Array
		for player in GameVariables.Players:
			if player != Player:
				var Username = GameVariables.Players[player]["Username"]
				if Username != "Dormant" and Username != "Empty":
					ChatPlayers.append(player)
		if ChatPlayers.empty() == false:
			var PlayerPills : Array
			for player in ChatPlayers:
				var PlayerPillInstance := PlayerPill.instance()
				PlayerPillInstance.set_player(player)
				PlayerPills.append(PlayerPillInstance)
			add_player_pills_to_rows(PlayerPills)
			for playerpill in PlayerPills:
				playerpill.get_node("PlayerButton").disabled = false
				playerpill.UpdateMessages = false
				playerpill.get_node("PlayerButton").pressed = false
				playerpill.UpdateMessages = true
		else:
			GameVariables.GameNode.show_popup("AbsentChatPlayers")
			$Chats.show()
			$Messages.hide()
	else:
		var PlayerPills : Array
		for player in players:
			if player != Player:
				var PlayerPillInstance := PlayerPill.instance()
				PlayerPillInstance.set_player(player)
				PlayerPills.append(PlayerPillInstance)
		add_player_pills_to_rows(PlayerPills)

func set_messages(players : Array) -> void:
	var Player : String = GameVariables.Player
	CurrentPlayers = players.duplicate(true)
	for child in $Messages/ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("Message"):
			child.queue_free()
	if GameVariables.Chats.has(players):
		for message in GameVariables.Chats[players]:
			var MessageInstance := Message.instance()
			MessageInstance.set_message(message)
			$Messages/ScrollContainer/VBoxContainer.add_child(MessageInstance)
	elif players != [Player]:
		# Failure case
		print(str(players) + " are not in an existing chat")

func set_chat(players : Array, playerselection : bool = false) -> void:
	$Chats.hide()
	$Messages.show()
	set_players(players, playerselection)
	set_messages(players)

func add_player_pills_to_rows(playerpills : Array) -> void:
	for child in $Messages/LargePanelHeader/PlayerPills.get_children():
		child.queue_free()
	$Messages/LargePanelHeader/PlayerPills.add_child(PlayerPillsContainer.instance()) 
	for playerpill in playerpills:
		var PlayerPillWidth : int = playerpill.get_node("PlayerLabel").get_font("font").get_string_size(playerpill.Username).x + 10
		var PlayerPillRowWidth : int = PlayerPillWidth
		for child in $Messages/LargePanelHeader/PlayerPills.get_children()[-1].get_children():
			PlayerPillRowWidth += child.rect_size.x
		if PlayerPillRowWidth > $Messages/LargePanelHeader/PlayerPills.rect_size.x:
			$Messages/LargePanelHeader/PlayerPills.add_child(PlayerPillsContainer.instance())
		$Messages/LargePanelHeader/PlayerPills.get_children()[-1].add_child(playerpill)

func send_message() -> void:
	# Checks that message is valid
	if (
		$Messages/TextEdit.text != "" and
		CurrentPlayers != [GameVariables.Player]
	):
		var NewMessage : Dictionary = {
			"Players" : [GameVariables.Player],
			"Text" : $Messages/TextEdit.text,
			"Time" : OS.get_unix_time()
		}
		Network.rpc_id(
			1,
			"send_message",
			# Game ID
			GameVariables.GameId,
			# Client ID
			get_tree().get_network_unique_id(),
			# Chat
			CurrentPlayers,
			# Message
			NewMessage
		)
		# Adds new message to chats
		if GameVariables.Chats.has(CurrentPlayers) == false:
			GameVariables.Chats[CurrentPlayers] = []
		GameVariables.Chats[CurrentPlayers].append(NewMessage)
		# Updates to display new message
		set_chat(CurrentPlayers)
		# Clear text input for new message
		$Messages/TextEdit.text = ""

func update_messages(players : Array) -> void:
	if players == CurrentPlayers:
		set_messages(CurrentPlayers)
	update_chats()

func jump_to_messages_end() -> void:
	if $Messages/ScrollContainer.get_v_scrollbar().max_value != MaximumScroll:
		MaximumScroll = $Messages/ScrollContainer.get_v_scrollbar().max_value
		$Messages/ScrollContainer.scroll_vertical = MaximumScroll

func _on_CreateChatButton_pressed() -> void:
	$Chats.hide()
	$Messages.show()
	var Player : String = GameVariables.Player
	set_chat([Player], true)

func _on_BackButton_pressed() -> void:
	$Chats.show()
	$Messages.hide()
	update_chats()

func _on_SendButton_pressed() -> void:
	send_message()

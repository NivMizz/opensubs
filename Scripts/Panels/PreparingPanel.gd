extends Panel

onready var LaunchingPanel : Node = get_node("/root/Game/Interface/Panels/Launching")
onready var SubmarinePanel : Node = get_node("/root/Game/Interface/Panels/Submarine")

var Outpost : String
var Submarine : String
var TemporaryTroops : bool = false

func _process(delta):
	if visible:
		if $ScrollWheel.pressed:
			if Settings.CalculateTroopsDelay == -1:
				update_submarine_troops()
			else:
				$CalculateTimer.start(Settings.CalculateTroopsDelay)

func prepare() -> void:
	TemporaryTroops = false
	# Get submarine data
	var SubmarineData : Dictionary = GameVariables.SubmarineData[Submarine]
	var SubmarineHistory : Array = SubmarineData["History"]
	var SubmarineHistoryEntry : Dictionary = HistoryFunctions.get_history_entry(GameVariables.VisibleTick, SubmarineHistory)
	update_tick()
	if $ScrollWheel.StepValue != SubmarineHistoryEntry["TroopsTotal"]:
		if $CalculateTimer.is_stopped():
			$ScrollWheel.set_value(SubmarineHistoryEntry["TroopsTotal"])
			$ScrollWheel.update_notch_positions()

func update_tick() -> void:
	if visible:
		# Get outpost data
		var OutpostData : Dictionary = GameVariables.OutpostData[Outpost]
		var OutpostHistory : Array = OutpostData["History"]
		var OutpostHistoryEntry : Dictionary = OutpostHistory[GameVariables.VisibleTick]
		# Get available troops
		var AvailableTroops : int = get_available_troops()
		# Set header elements
		$Header.modulate = Color(GameVariables.Colors[GameVariables.Player])
		$OutpostLabel.text = OutpostHistoryEntry["CurrentType"].to_upper()
		# Sets scroll wheel
		$ScrollWheel.MaximumValue = AvailableTroops
		# Set troops text
		if TemporaryTroops == false:
			# Get submarine data
			var SubmarineData : Dictionary = GameVariables.SubmarineData[Submarine]
			var LaunchOrder : Dictionary = GameVariables.Orders[SubmarineData["LaunchOrderTick"]][SubmarineData["LaunchOrderId"]]
			# Set troops text
			$TroopsLabel.text = str(AvailableTroops - LaunchOrder["InitialTroops"])

func update_submarine_troops() -> void:
	# Get submarine data
	var Data : Dictionary = GameVariables.SubmarineData[Submarine]
	var LaunchOrder : Dictionary = GameVariables.Orders[Data["LaunchOrderTick"]][Data["LaunchOrderId"]]
	# Check submarine has not been cancelled
	if GameVariables.Orders.has(LaunchOrder["Tick"]) and GameVariables.Orders[LaunchOrder["Tick"]].has(LaunchOrder["OrderId"]):
		# Stops calculate timer
		$CalculateTimer.disconnect("timeout", self, "_on_CalculateTimer_timeout")
		$CalculateTimer.stop()
		$CalculateTimer.connect("timeout", self, "_on_CalculateTimer_timeout")
		# If scrollwheel has changed, update submarine troops
		if $ScrollWheel.StepValue != LaunchOrder["InitialTroops"]:
			Network.rpc_id(
				1,
				"set_launch_order_troops",
				# Game ID
				GameVariables.GameId,
				# Client ID
				get_tree().get_network_unique_id(),
				# Tick
				LaunchOrder["Tick"],
				# Launch order ID
				LaunchOrder["OrderId"],
				# Troops
				$ScrollWheel.StepValue
			)
			OrderFunctions.set_launch_order_troops(LaunchOrder, $ScrollWheel.StepValue)

func set_temporary_troops(troops : int) -> void:
	TemporaryTroops = true
	$TroopsLabel.text = str(troops)

func set_temporary_outpost_troops(troops : int) -> void:
	GameVariables.GameNode.set_outpost_temporary_troops(Outpost, troops)
	set_temporary_troops(troops)

func set_temporary_submarine_troops(troops : int) -> void:
	GameVariables.GameNode.set_submarine_temporary_troops(Submarine, troops)
	LaunchingPanel.set_temporary_troops(troops)

func cancel_submarine() -> void:
	# Get orders for removal
	var RemovalOrders : Array = OrderFunctions.get_all_submarine_orders(Submarine)
	# Remove orders
	for order in RemovalOrders:
		Network.rpc_id(
			1,
			"remove_order",
			# Game ID
			GameVariables.GameId,
			# Client ID
			get_tree().get_network_unique_id(),
			# Tick
			order["Tick"],
			# Order ID
			order["OrderId"]
		)
		OrderFunctions.remove_order(order)
	$CalculateTimer.stop()

func get_available_troops() -> int:
	# Get outpost data
	var OutpostData : Dictionary = GameVariables.OutpostData[Outpost]
	var OutpostHistory : Array = OutpostData["History"]
	var OutpostHistoryEntry : Dictionary = OutpostHistory[GameVariables.VisibleTick]
	# Get outpost submarine troops
	var OutpostSubmarineSourceTick : int = DataFunctions.get_data_total_previous_source_tick(GameVariables.VisibleTick, OutpostData, "Troops", "TroopsKeys", Submarine)
	var OutpostSubmarineValue : int = DataFunctions.get_data_total_source_value(OutpostSubmarineSourceTick, OutpostData, "Troops", Submarine)
	return OutpostHistoryEntry["TroopsTotal"] - OutpostSubmarineValue

func _on_CancelButton_pressed() -> void:
	cancel_submarine()

func _on_CalculateTimer_timeout() -> void:
	update_submarine_troops()

func _on_ScrollWheel_value_changed(value : float):
	set_temporary_outpost_troops(get_available_troops() - value)
	set_temporary_submarine_troops(value)

extends Panel

var PlayerLeaderboardEntry : PackedScene = preload("res://Scenes/InterfaceElements/PlayerLeaderboardEntry.tscn")

var PlayerInfoType : String = "Troops"

func _ready() -> void:
	for player in GameVariables.Players:
		if player != "Dormant":
			var PlayerLeaderboardEntryInstance := PlayerLeaderboardEntry.instance()
			PlayerLeaderboardEntryInstance.set_player(player)
			$ScrollContainer/VBoxContainer.add_child(PlayerLeaderboardEntryInstance)
			$ScrollContainer/VBoxContainer.move_child(PlayerLeaderboardEntryInstance, 0)

func update_tick() -> void:
	for child in $ScrollContainer/VBoxContainer.get_children():
		if child.is_in_group("Permanent") == false:
			child.update_tick()

func set_player_info_type(playerinfotype : String) -> void:
	PlayerInfoType = playerinfotype
	for child in $PlayerInfoTypes.get_children():
		child.pressed = child.name == PlayerInfoType
	update_tick()

func _on_Troops_pressed():
	set_player_info_type("Troops")

func _on_Outposts_pressed():
	set_player_info_type("Outposts")

func _on_Mining_pressed():
	set_player_info_type("Mining")

extends Panel

var Submarine : String
var LaunchOrder : Dictionary

func _ready() -> void:
	hide()

func prepare() -> void:
	update_tick()

func update_tick() -> void:
	if visible:
		# Get submarine data
		var Data : Dictionary = GameVariables.SubmarineData[Submarine]
		var History : Array = Data["History"]
		var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(GameVariables.VisibleTick, History)
		LaunchOrder = GameVariables.Orders[Data["LaunchOrderTick"]][Data["LaunchOrderId"]]
		# Set header elements
		var Player : String = HistoryEntry["CurrentPlayer"]
		$Header.modulate = Color(GameVariables.Colors[Player])
		$PlayerLabel.text = GameVariables.Players[Player]["Username"]

func _on_ClockButton_pressed() -> void:
	# Check if order is invalid
	if OrderFunctions.get_order_invalid(LaunchOrder) == false:
		GameVariables.GameNode.jump_to_arrival(Submarine)

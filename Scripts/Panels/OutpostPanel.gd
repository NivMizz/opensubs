extends Panel

var Outpost : String

func _ready() -> void:
	hide()

func prepare() -> void:
	update_tick()

func update_tick() -> void:
	if Outpost != "":
		# Get outpost data
		var OutpostData : Dictionary = GameVariables.OutpostData[Outpost]
		var OutpostHistory : Array = OutpostData["History"]
		var OutpostHistoryEntry : Dictionary = OutpostHistory[GameVariables.VisibleTick]
		# Set header elements
		var Player : String = OutpostHistoryEntry["CurrentPlayer"]
		$Header.modulate = Color(GameVariables.Colors[Player])
		$OutpostLabel.text = OutpostHistoryEntry["CurrentType"].to_upper()
		$PlayerLabel.text = GameVariables.Players[Player]["Username"]
		$PlayerLabel.visible = GameVariables.Players[Player]["Username"] != "Empty"
		# Sets outpost info
		$Info/TroopsLabel.text = str(OutpostHistoryEntry["TroopsTotal"])
		$Info/InfoLabel.text = "Drillers" + get_outpost_info()

func get_outpost_info() -> String:
	return ""

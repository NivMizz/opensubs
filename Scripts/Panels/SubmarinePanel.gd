extends Panel

var Submarine : String
var LaunchOrder : Dictionary
var GiftOrder : Dictionary
var VisibleToasts : Array
var HistoryEntry : Dictionary
var TemporaryTroops : bool = false

func _process(delta):
	if visible:
		# Sets launching toast
		if VisibleToasts.has("Launching"):
			var TimeToLaunch : int = OrderFunctions.get_submarine_time_to_order_effect(LaunchOrder)
			if TimeToLaunch != 0:
				$Toasts/Launching/ToastLabel.text = "Launching in " + VariableFunctions.get_time_text(TimeToLaunch)
			else:
				$Toasts/Launching.hide()
		else:
			$Toasts/Launching.hide()
		# Sets gift toast
		$GiftButton.visible = HistoryEntry["CurrentPlayer"] == GameVariables.Player
		$Toasts/Gifting.show()
		if VisibleToasts.has("Gifting"):
			var TimeToGift : int = OrderFunctions.get_submarine_time_to_order_effect(GiftOrder)
			if TimeToGift != 0:
				$Toasts/Gifting/ToastLabel.text = "Gifting in " + VariableFunctions.get_time_text(TimeToGift)
			else:
				$GiftButton.hide()
				$Toasts/Gifting.hide()
		else:
			$Toasts/Gifting.hide()

func prepare() -> void:
	TemporaryTroops = false
	update_tick()

func update_tick() -> void:
	if visible:
		# Get submarine data
		var Data : Dictionary = GameVariables.SubmarineData[Submarine]
		var History : Array = Data["History"]
		HistoryEntry = HistoryFunctions.get_history_entry(GameVariables.VisibleTick, History)
		# Set header elements
		var Player : String = HistoryEntry["CurrentPlayer"]
		$Header.modulate = Color(GameVariables.Colors[Player])
		$PlayerLabel.text = GameVariables.Players[Player]["Username"]
		# Sets submarine info
		$Info/InfoLabel.text = GameVariables.GameNode.get_submarine_info(Submarine)
		# Gets submarine launch order
		LaunchOrder = GameVariables.Orders[Data["LaunchOrderTick"]][Data["LaunchOrderId"]]
		# Gets submarine gift order
		GiftOrder = OrderFunctions.get_awaiting_effect_order(Data["Gifts"], Data["GiftsKeys"], "Gift")
		# Sets troop panel elements
		if TemporaryTroops == false:
			if OrderFunctions.get_order_invalid(LaunchOrder) == false:
				$Info/TroopsLabel.text = str(HistoryEntry["TroopsTotal"])
			else:
				$Info/TroopsLabel.text = str(LaunchOrder["InitialTroops"])
		# Sets toast panel elements
		set_submarine_toasts(Submarine)
		# Sets gift panel button pressed
		$GiftButton.disconnect("toggled", self, "_on_GiftButton_toggled")
		$GiftButton.pressed = GiftOrder.empty() == false and OrderFunctions.get_order_awaiting_effect(GiftOrder)
		$GiftButton.connect("toggled", self, "_on_GiftButton_toggled")
		# Sets submarine label text
		if HistoryEntry["CurrentGift"]:
			$SubmarineLabel.text = "SUB (GIFT)"
		else:
			$SubmarineLabel.text = "SUB"

# Gets submarine toasts that should be visible
func set_submarine_toasts(toastsubmarine : String) -> void:
	if visible:
		if toastsubmarine == Submarine:
			var Toasts : Array
			# Checks if launch toast should be shown
			var TimeToLaunch : int = OrderFunctions.get_submarine_time_to_order_effect(LaunchOrder)
			if TimeToLaunch != 0:
				Toasts.append("Launching")
			# Checks if gift toast should be shown
			if GiftOrder.empty() == false:
				var TimeToGift : int = OrderFunctions.get_submarine_time_to_order_effect(GiftOrder)
				if TimeToGift <= GameVariables.GiftWaitTicks * GameVariables.TickLength and TimeToGift != 0:
					Toasts.append("Gifting")
			# Check for toasts associated with orders
			for order in [LaunchOrder, GiftOrder]:
				if order.empty() == false:
					# If order is invalid, add invalid reasons to toasts
					if OrderFunctions.get_order_invalid(order):
						Toasts.append_array(GameVariables.InvalidOrders[order["Tick"]][order["OrderId"]])
					# If order is warning, add warning reasons to toasts
					if OrderFunctions.get_order_warning(order):
						Toasts.append_array(GameVariables.WarningOrders[order["Tick"]][order["OrderId"]])
			# Sets panel toasts
			VisibleToasts = Toasts
			for child in $Toasts.get_children():
				child.visible = VisibleToasts.has(child.name)

func set_temporary_troops(troops : int) -> void:
	TemporaryTroops = true
	$Info/TroopsLabel.text = str(troops)

func _on_GiftButton_toggled(buttonpressed : bool) -> void:
	# Get submarine data
	var Data : Dictionary = GameVariables.SubmarineData[Submarine]
	var LaunchOrder : Dictionary = GameVariables.Orders[Data["LaunchOrderTick"]][Data["LaunchOrderId"]]
	# Check if visible tick is valid for order
	if GameVariables.VisibleTick >= GameVariables.GlobalTick:
		if buttonpressed:
			var OrderId : int = randi()
			var GiftTime : int = -1
			if GameVariables.StartTime != -1:
				GiftTime = OS.get_unix_time()
			var Order : Dictionary = {
				"LaunchOrderTick" : LaunchOrder["Tick"],
				"LaunchOrderId" : LaunchOrder["OrderId"],
				"Player" : GameVariables.Player,
				"Time" : GiftTime,
				"Type" : "Gift",
			}
			Network.rpc_id(
				1,
				"set_order",
				# Game ID
				GameVariables.GameId,
				# Client ID
				get_tree().get_network_unique_id(),
				# Tick
				GameVariables.VisibleTick,
				# Order ID
				OrderId,
				# Order
				Order
			)
			OrderFunctions.set_order(GameVariables.VisibleTick, OrderId, Order)
		else:
			var GiftOrder : Dictionary = OrderFunctions.get_awaiting_effect_order(Data["GiftOrders"], Data["GiftOrdersKeys"], "Gift")
			Network.rpc_id(
				1,
				"remove_order",
				# Game ID
				GameVariables.GameId,
				# Client ID
				get_tree().get_network_unique_id(),
				# Tick
				GiftOrder["Tick"],
				# Order ID
				GiftOrder["OrderId"]
			)
			OrderFunctions.remove_order(GiftOrder)
	update_tick()

func _on_ClockButton_pressed() -> void:
	# Check if order is valid
	if OrderFunctions.get_order_invalid(LaunchOrder) == false:
		GameVariables.GameNode.jump_to_arrival(Submarine)

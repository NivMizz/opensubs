extends Node2D

onready var PreparingPanel : Node = get_node("/root/Game/Interface/Panels/Preparing")
onready var LaunchingPanel : Node = get_node("/root/Game/Interface/Panels/Launching")
onready var SubmarinePanel : Node = get_node("/root/Game/Interface/Panels/Submarine")

export var OutlineWidth : int = 15

var SubmarineLines : PackedScene = preload("res://Scenes/Lines/SubmarineLines.tscn")

var History : Array
var LaunchOrderTick : int
var LaunchOrderId : int
var GiftOrders : Dictionary
var GiftOrdersKeys : Array
var InitialOutpost : String
var InitialPlayer : String
var SubmarineLinesInstance : Node
var Selected : bool = false
var TemporaryTroops : bool = false
var Cancelled : bool = false

# Inbuilt funciton

func _ready() -> void:
	$Body/Sprites.set_material(Shaders.Submarine.duplicate())
	$Body/Sprites.material.set_shader_param("width", OutlineWidth)

# Start functions

# Initial setup
func prepare() -> void:
	SubmarineLinesInstance = SubmarineLines.instance()
	SubmarineLinesInstance.Submarine = name
	GameVariables.GameNode.get_node("Lines").add_child(SubmarineLinesInstance)
	update_tick()

# Tick functions

# Updates the submarine to show it's state at the passed tick
func update_tick() -> void:
	if Cancelled == false:
		# Get data
		var HistoryIndex : int = HistoryFunctions.get_history_index(GameVariables.VisibleTick, History)
		var HistoryEntry : Dictionary = History[HistoryIndex]
		# Set state
		if HistoryIndex != -1:
			$Body/Sprites.material.set_shader_param("bodycolor", Color(GameVariables.Colors.get(HistoryEntry["CurrentPlayer"])))
			update_position()
			show()
			set_panel()
			var LaunchOrder : Dictionary = GameVariables.Orders[LaunchOrderTick][LaunchOrderId]
			var AwaitingLaunch : bool = OrderFunctions.get_order_awaiting_effect(LaunchOrder)
			# Set submarine visible
			if HistoryEntry["VisibleTotal"] <= 0:
				hide_submarine()
			# Check if order is valid
			elif OrderFunctions.get_order_invalid(LaunchOrder) == false:
				# Set troops label
				if TemporaryTroops == false:
					$Body/TroopsLabel.text = str(HistoryEntry["TroopsTotal"])
				# Check if submarine is launching
				if AwaitingLaunch:
					SubmarineLinesInstance.switch_line("Preparing")
					if $PrepareMoveAnimation.is_playing() == false:
						$PrepareMoveAnimation.play("PrepareMove")
				else:
					SubmarineLinesInstance.switch_line("Launched")
					$PrepareMoveAnimation.stop()
					$Body.position = Vector2()
			else:
				# Set troops label
				if TemporaryTroops == false:
					$Body/TroopsLabel.text = str(LaunchOrder["InitialTroops"])
				# Check if submarine is launching
				if AwaitingLaunch:
					if $PrepareMoveAnimation.is_playing() == false:
						$PrepareMoveAnimation.play("PrepareMove")
					SubmarineLinesInstance.switch_line("Invalid")
					var InvalidReasons : Array = GameVariables.InvalidOrders[LaunchOrder["Tick"]][LaunchOrder["OrderId"]]
					if InvalidReasons.has("OutpostCaptured"):
						var InitialOutpostData : Dictionary = GameVariables.OutpostData[InitialOutpost]
						var InitialOutpostHistory : Array = InitialOutpostData["History"]
						var InitialOutpostHistoryEntry : Dictionary = HistoryFunctions.get_history_entry(GameVariables.VisibleTick, InitialOutpostHistory)
						if InitialOutpostHistoryEntry["CurrentPlayer"] != HistoryEntry["CurrentPlayer"]:
							hide_submarine()
				else:
					hide_submarine()
			# Check if submarine should play gifting animation
			var GiftOrder : Dictionary = OrderFunctions.get_awaiting_effect_order(GiftOrders, GiftOrdersKeys, "Gift")
			if GiftOrder.empty() == false:
				$GiftFlashAnimation.play("GiftFlash")
			else:
				# Set submarine sprite
				$GiftFlashAnimation.stop()
				$Body/Sprites/GiftSprite.visible = HistoryEntry["CurrentGift"]
				$Body/Sprites/SubmarineSprite.visible = not HistoryEntry["CurrentGift"]
		else:
			hide_submarine()
			if Selected:
				if GameVariables.VisibleTick > History[-1]["Tick"]:
					# TODO: make work with submarines
					GameVariables.GameNode.focus_outpost(History[-1]["CurrentTarget"])

# Data functions

# Points submarine in direction of target
func set_course(targetposition : Vector2) -> void:
	var DirectionAngle : float = global_position.angle_to_point(targetposition)
	rotation = DirectionAngle - PI
	var Flipped : bool = rad2deg(DirectionAngle) < 90 and rad2deg(DirectionAngle) > -90
	if Flipped:
		$Body.scale.y = -1
		$Body/TroopsLabel.rect_scale.x = -1
	else:
		$Body.scale.y = 1
		$Body/TroopsLabel.rect_scale.x = 1

# Sends remove submarine call
func cancel_submarine() -> void:
	Cancelled = true
	SubmarineLinesInstance.LineButtonsInstance.queue_free()
	SubmarineLinesInstance.queue_free()
	queue_free()

# Jumps time controller to submarine arrival
func jump_to_arrival() -> void:
	GameVariables.GameNode.jump_to_tick(History[-1]["Tick"] + 1)

# Visual functions

# Sets the submarine's current position and rotation
func update_position() -> void:
	var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(GameVariables.VisibleTick, History)
	if HistoryEntry.empty() == false:
		var WrappedPosition : Vector2 = MapFunctions.get_wrapped_position(HistoryEntry["Position"])
		global_position = WrappedPosition
		# TODO: make work with submarines
		var TargetData : Dictionary = GameVariables.OutpostData[HistoryEntry["CurrentTarget"]]
		var TargetPosition : Vector2 = MapFunctions.get_closest_wrapped_position(HistoryEntry["Position"], TargetData["InitialPosition"])
		var RelativeTargetPosition : Vector2 = global_position - (HistoryEntry["Position"] - TargetPosition)
		SubmarineLinesInstance.set_line_points([global_position, RelativeTargetPosition])
		set_course(RelativeTargetPosition)

# Sets the correct panels to be visible
func set_panel() -> void:
	if Selected:
		var LaunchOrder : Dictionary = GameVariables.Orders[LaunchOrderTick][LaunchOrderId]
		var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(GameVariables.VisibleTick, History)
		if HistoryEntry["VisibleTotal"] <= 0:
			GameVariables.GameNode.show_panel("ObscuredSubmarine")
		elif (
			HistoryEntry["CurrentPlayer"] == GameVariables.Player and
			OrderFunctions.get_order_awaiting_effect(LaunchOrder)
		):
			GameVariables.GameNode.switch_submarine_panels(["Preparing", "Launching"])
		else:
			GameVariables.GameNode.switch_submarine_panels(["Submarine"])

# Sets visual focused effects
func focus() -> void:
	Selected = true
	set_panel()
	$Body/Sprites.material.set_shader_param("selected", true)

# Sets visual unfocused effects
func unfocus() -> void:
	Selected = false
	$Body/Sprites.material.set_shader_param("selected", false)

# Hides the submarine
func hide_submarine() -> void:
	if Selected:
		GameVariables.GameNode.switch_submarine_panels([])
	unfocus()
	SubmarineLinesInstance.hide_lines()
	hide()

# Temporarily changes troops
func set_temporary_troops(troops) -> void:
	TemporaryTroops = true
	$Body/TroopsLabel.text = str(troops)

# Signals

# Makes submarine panels visible
func _on_Button_pressed() -> void:
	GameVariables.GameNode.focus_submarine(name)

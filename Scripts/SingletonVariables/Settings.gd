extends Node

# User settings

var Username : String
var ServerIp : String
var ServerPort : int
var CalculateTroopsDelay : int = 0.2
var VisibleTickMaximumAhead : int = 300

# Hidden settings

var ConnectionTimeoutLength : int = 3

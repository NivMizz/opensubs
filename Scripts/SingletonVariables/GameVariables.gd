extends Node

var Colors : Dictionary = {
	"Dormant" : "4C4C4C",
	"Red" : "AB2024",
	"Orange" : "D28E2A",
	"Cyan" : "5C9A8C",
	"Sky" : "7BA7D8",
	"Blue" : "3A4BA3",
	"Purple" : "6A558E",
	"Pink" : "C777B1",
	"Olive" : "686E47",
	"Beige" : "9B907B",
	"Brown" : "8B5E3B"
}

# Default game setup variables

var DefaultGenerateTroopsTicks : int = 50
var DefaultGenerateMaximumShieldTicks : int = 300
var DefaultSonarRange : float = 400.0
var DefaultSubmarineSpeed : float = 5.0
var DefaultChargePerGenerator : int = 120
var TickLengthsList : Array = [1, 5, 10, 30, 60, 300, 600]
var MapTypesList : Array = ["RandomSpread", "Grid"]
var OutpostNamesList : Array = ["Classics", "Capitals"]

# Game setup variables

# Basic setup variables
var TickLength : int
var MapType : String
var OutpostsPerPlayer : int
var GenerateTroopsTicksDivisor : float
var GenerateMaximumShieldTicksDivisor : float
var SonarRangeMultiplier : float
var SubmarineSpeedMultiplier : int
# Advanced setup variables
var OutpostNames : String
var LaunchWaitTicks : int
var GiftWaitTicks : int
# Unimplemented setup variables
var OutpostSpacing : int
var OutpostStartingTroops : int
var GenerateTroopsNumber : int

# Game state variables

# Game variables
var GameId : int
var InitialCalculation : bool = false
# Time variables
var StartTime : int = -1
# Tick variables
var GlobalTick : int = 0
var VisibleTick : int = 0
var VisibleTickMaximum : int
# Generation variables
var ActiveOutposts : Array
var ActiveSubmarines : Array
var CurrentActiveSubmarines : Array
var GenerateTroopsTicks : int
var GenerateMaximumShieldTicks : int
# Order variables
var Orders : Dictionary
var OrdersKeys : Array
var InvalidOrders : Dictionary
var WarningOrders : Dictionary
# Data variables
var OutpostData : Dictionary
var SubmarineData : Dictionary
# Players variables
var Players : Dictionary
var PlayersInfo : Dictionary
var Player : String
var OnlinePlayers : Array
var Chats : Dictionary
# Map variables
var MapSize : Vector2
var HalfMapSize : Vector2

# Node variables

var GameNode : Node

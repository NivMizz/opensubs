extends Control

export var SelectedSizeIncrease : int = 1.1
export var OutpostCursorMovementAllowance : float = 10.0

var Selected : bool = false
var Focused : bool = false
var InitialPosition : Vector2
var InitialShieldMaximum : int
var History : Array
var PreviousOutpostCursorPosition : Vector2
var Submarines : Dictionary
var TemporaryTroops : bool = false

# Inbuilt functions

func _ready() -> void:
	$Shield/ShieldRings.set_material(Shaders.ShieldRing.duplicate())
	$BlockMoveAnimation.play("BlockMove")
	$Labels/NameLabel.text = name
	rect_global_position = InitialPosition - rect_pivot_offset
	update_tick()

func _process(_delta : float) -> void:
	if $Button.pressed:
		if GameVariables.GameNode.HoveredStartOutpost != self:
			var OutpostCursorMovementDifference : float = PreviousOutpostCursorPosition.distance_to(get_global_mouse_position())
			if OutpostCursorMovementDifference >= OutpostCursorMovementAllowance:
				GameVariables.GameNode.set_hovered_start_outpost(self)

# Tick functions

func update_tick() -> void:
	var HistoryEntry : Dictionary = History[GameVariables.VisibleTick]
	# Checks if outpost should be obscured
	var CurrentType : String = get_current_type(GameVariables.VisibleTick)
	# Sets current outpost type sprite to visible
	for child in $Sprites/Outpost.get_children():
		child.visible = child.name == CurrentType
	# Sets current outpost type production blocks to visible
	for child in $Sprites/ProductionBlocks.get_children():
		child.visible = child.name == CurrentType
	# Sets current outpost type panel
	if Selected:
		GameVariables.GameNode.switch_outpost_panels([CurrentType])
	# Sets outpost color
	$Sprites/Outpost.modulate = Color(GameVariables.Colors[HistoryEntry["CurrentPlayer"]])
	# Shows outpost details if not obscured
	$SonarRangeMasks.update_masks()
	if CurrentType != "ObscuredOutpost":
		# Sets troops label text to current troops total
		if TemporaryTroops == false:
			$Labels/TroopsLabel.text = str(HistoryEntry["TroopsTotal"])
		$Labels/TroopsLabel.show()
		$Shield.show()
		update_shield_rings()
	else:
		$Labels/TroopsLabel.hide()
		$Shield.hide()

# Visual functions

# Sets the outpost's current position
func update_position() -> void:
	rect_global_position = MapFunctions.get_wrapped_position(rect_global_position + rect_pivot_offset) - rect_pivot_offset

func focus() -> void:
	if Focused == false:
		$FocusAnimation.play("Focus")
	Focused = true

func unfocus() -> void:
	if Focused:
		$FocusAnimation.play("Unfocus")
	Focused = false

func get_current_type(tick : int) -> String:
	var HistoryEntry : Dictionary = HistoryFunctions.get_history_entry(tick, History)
	var CurrentType : String = HistoryEntry["CurrentType"]
	if HistoryEntry["VisibleTotal"] <= 0:
		CurrentType = "ObscuredOutpost"
	return CurrentType

# Temporarily change troops
func set_temporary_troops(troops : int) -> void:
	TemporaryTroops = true
	$Labels/TroopsLabel.text = str(troops)

func update_shield_rings() -> void:
	var HistoryEntry : Dictionary = History[GameVariables.VisibleTick]
	var ShieldMaximum : int = InitialShieldMaximum * HistoryEntry["ShieldMaximumMultipliersTotal"]
	var ShieldRingNumber : int = ceil(ShieldMaximum / 10)
	# Sets shield rings visibility
	for child in $Shield/ShieldRings.get_children():
		child.visible = child.get_position_in_parent() == ShieldRingNumber - 1
	# Sets empty shield rings visibility
	for child in $Shield/EmptyShieldRings.get_children():
		child.visible = child.get_position_in_parent() == ShieldRingNumber - 1
	# Sets shield notches and shield label visibility
	if HistoryEntry["ShieldTotal"] > 0:
		$Shield/ShieldLabel.show()
		$Shield/ShieldNotches.show()
		for child in $Shield/ShieldNotches.get_children():
			child.visible = child.get_position_in_parent() == ShieldRingNumber - 1
	else:
		$Shield/ShieldLabel.hide()
		$Shield/ShieldNotches.hide()
	# Checks if a ring should be displayed
	if ShieldRingNumber > 0:
		# Set shield ring
		# Shield total / shield maximum * visible shield ring fraction (1.0 - 60.0 / 360.0) + shield ring start offset (30.0 / 360.0)
		var FillRatio : float = float(HistoryEntry["ShieldTotal"]) / float(ShieldMaximum) * 0.8333 + 0.0833
		$Shield/ShieldRings.material.set_shader_param("fill_ratio", FillRatio)
		if HistoryEntry["ShieldTotal"] > 0:
			# Set shield notch
			var NotchRotation : float = 360.0 * FillRatio
			$Shield/ShieldNotches.rect_rotation = NotchRotation
			# Set shield label
			$Shield/ShieldLabel.rect_position = $Shield.rect_size / 2 + Vector2(100 + 15 * (ShieldRingNumber - 1), 0).rotated(deg2rad(NotchRotation)) - $Shield/ShieldLabel.rect_pivot_offset
			$Shield/ShieldLabel.text = str(HistoryEntry["ShieldTotal"])

# Signals

func _on_Button_button_down() -> void:
	PreviousOutpostCursorPosition = get_global_mouse_position()

func _on_Button_button_up() -> void:
	if GameVariables.GameNode.HoveredStartOutpost != self:
		GameVariables.GameNode.focus_outpost(name)

func _on_FocusArea_mouse_entered() -> void:
	focus()
	GameVariables.GameNode.HoveredEndOutpost = self
	GameVariables.GameNode.CursorPosition = InitialPosition

func _on_FocusArea_mouse_exited() -> void:
	unfocus()
	if GameVariables.GameNode.HoveredEndOutpost == self:
		GameVariables.GameNode.HoveredEndOutpost = null

extends Node2D

func _ready():
	for x in 3:
		for y in 3:
			var SonarRangeMask : Node = get_child(x * 3 + y)
			# Set mask position
			var WrapOffset : Vector2 = Vector2(x - 1, y - 1) * GameVariables.MapSize
			SonarRangeMask.position = WrapOffset

func update_masks() -> void:
	# Gets data
	var History : Array = get_parent().History
	var HistoryEntry : Dictionary = History[GameVariables.GlobalTick]
	# Get mask size
	# 1 / texture size * sonar range radius * 2 (make diameter) * sonar range mutiplier
	var SonarRangeTextureScale : float = 1.0 / 500.0 * GameVariables.DefaultSonarRange * GameVariables.SonarRangeMultiplier * 2 * HistoryEntry["SonarRangeMultipliersTotal"]
	# Get mask enabled
	var SonarRangeEnabled : bool = SonarFunctions.get_sonar_item_status(GameVariables.VisibleTick, get_parent().name)
	# Updates sonar masks
	if SonarRangeEnabled:
		if get_child(0).enabled == false:
			for child in get_children():
				# Set mask size
				child.texture_scale = SonarRangeTextureScale
				# Set mask enabled
				child.enabled = true
		else:
			for child in get_children():
				# Set mask size
				child.texture_scale = SonarRangeTextureScale
	else:
		if get_child(0).enabled:
			for child in get_children():
				# Set mask enabled
				child.enabled = false

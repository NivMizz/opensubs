extends Node2D

var RulerStartTick : int

func _process(_delta : float) -> void:
	if Input.is_action_just_released("Click"):
		if GameVariables.GameNode.HoveredStartOutpost != null:
			hide()
			GameVariables.GameNode.set_tick(RulerStartTick)
			GameVariables.GameNode.create_submarine()
			GameVariables.GameNode.HoveredStartOutpost = null
			GameVariables.GameNode.HoveredEndOutpost = null
	elif GameVariables.GameNode.HoveredStartOutpost != null:
		show()
		GameVariables.GameNode.show_panel("Time")
		var HoveredStartOutpostPosition : Vector2 = GameVariables.GameNode.HoveredStartOutpost.rect_global_position + GameVariables.GameNode.HoveredStartOutpost.rect_pivot_offset
		var CursorWrappedPosition : Vector2 = MapFunctions.get_closest_wrapped_position(HoveredStartOutpostPosition, GameVariables.GameNode.CursorPosition)
		var TicksToArrival : int = floor(HoveredStartOutpostPosition.distance_to(CursorWrappedPosition) / (GameVariables.DefaultSubmarineSpeed * GameVariables.SubmarineSpeedMultiplier) + GameVariables.LaunchWaitTicks)
		for x in 3:
			for y in 3:
				var Ruler : Node = get_child(x * 3 + y)
				# Set line points
				var WrapOffset : Vector2 = Vector2(x - 1, y - 1) * GameVariables.MapSize
				Ruler.get_node("RulerLine").points = [HoveredStartOutpostPosition + WrapOffset, CursorWrappedPosition + WrapOffset]
				# Set label text
				if GameVariables.GameNode.HoveredEndOutpost == null:
					Ruler.get_node("RulerLabel").rect_position = GameVariables.GameNode.CursorPosition - Ruler.get_node("RulerLabel").rect_pivot_offset - Vector2(0, 20)
				else:
					Ruler.get_node("RulerLabel").rect_position = GameVariables.GameNode.CursorPosition - Ruler.get_node("RulerLabel").rect_pivot_offset - Vector2(0, 80)
				Ruler.get_node("RulerLabel").text = str(VariableFunctions.get_time_text(TicksToArrival * GameVariables.TickLength))
		GameVariables.GameNode.set_tick(RulerStartTick + TicksToArrival)

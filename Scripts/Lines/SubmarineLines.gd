extends Node2D

var LineButtons : PackedScene = preload("res://Scenes/Lines/LineButtons.tscn")

var Submarine : String
var LineButtonsInstance : Node

func _ready() -> void:
	LineButtonsInstance = LineButtons.instance()
	GameVariables.GameNode.get_node("LineButtons").add_child(LineButtonsInstance)
	LineButtonsInstance.connect("pressed", self, "line_button_pressed")
	for x in 3:
		for y in 3:
			var Lines : Node = get_child(x * 3 + y)
			# Set line position
			var WrapOffset : Vector2 = Vector2(x - 1, y - 1) * GameVariables.MapSize
			Lines.position = WrapOffset

func line_button_pressed() -> void:
	GameVariables.GameNode.focus_submarine(Submarine)

func set_line_points(linepoints : Array) -> void:
	for child in get_children():
		for secondarychild in child.get_children():
			secondarychild.points = linepoints
	LineButtonsInstance.set_line_points(linepoints)

func switch_line(linestring : String) -> void:
	show_lines()
	for lines in get_children():
		for line in lines.get_children():
			line.visible = line.name == linestring

func show_lines() -> void:
	LineButtonsInstance.show()
	show()

func hide_lines() -> void:
	LineButtonsInstance.hide()
	hide()

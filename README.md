# Opensubs (name subject to change)

![Discord Server](https://img.shields.io/discord/987039798076252201?style=flat-square)

## Important

Note that the project is still in early development. It is not yet recommended that you attempt to create a dedicated server for it. If you wish to play, you should can host the client and server on your personal computer whilst you are playing. Bugs are to be expected, please check the [Taskade board](https://www.taskade.com/d/tmCV7XxY198XtMjD?share=edit&edit=shmZrNhuy1tpKUff) to see if they have been noted

## About 
This project is an attempt to remake the gameplay experience of the mobile strategy game [Subterfuge](http://subterfuge-game.com/) in the [Godot game engine](https://godotengine.org/). This project consists of two parts, the client (this repository) and the [server](https://codeberg.org/mossy/opensubs-server). This project is community-run and is not associated with the developers of the original game

## Contributing

If you wish to contribute, please do your best to stick to the style of code the project uses. This mainly consists of:

* Having statically typed variables
* Defining the return type of functions
* Functions should be named in `underscore_separated_lower_case`
* Function parameters should be named in `conjoinedlowercase`
* **Everything else** should be written in `UpperCamelCase`

The project has a [Taskade board](https://www.taskade.com/d/tmCV7XxY198XtMjD?share=edit&edit=shmZrNhuy1tpKUff) for organizing work that needs to be done

To start contributing:

1. Download and install the standard edition of Godot for your operating system [from the Godot website.](https://godotengine.org/download)
2. Fork the repository.
3. Create an SSH key and add it to your Codeberg account to be able to clone the repository. [See the instructions here.](https://docs.codeberg.org/security/ssh-key/)
4. Start Godot and import the project(s) once you have cloned them.

## Roadmap

The following is a rough roadmap of plans for future releases (subject to heavy change):

* 0.1 - Initial release ✅
* 0.2 - Lobby creation/customization ✅
* 0.3 - Map wrapping and sonar range ✅
* 0.4 - Order queuing/UI improvements ✅
* 0.5 - Generators and win conditions
* 0.6 - Specialists
